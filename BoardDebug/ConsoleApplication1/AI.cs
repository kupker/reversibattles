using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

/// <summary>
/// AI using AlphaBeta algorithm.
/// </summary>
public class AI
{
    Check color;

	Coordinates frameCenter;
	int neighborhoodDistance;
	int depth;
	List<Coordinates> staticSet;

    bool cancel;

	// Board. Should be set externaly before NextMove is called.
	// Reason that this is not a parameter of NextMove is that
	// in this way, NextMove can be called in another thread.
    public Board boardState;

	// Next move of AI. This should be checked after NextMove is called.
	// This is not return value of NextMove for the same reason as above.
    public Coordinates nextMove;

	/// <summary>
	/// Initializes a new instance of the <see cref="AI"/> class.
	/// </summary>
	/// <param name="color">Color of stones of AIs.</param>
    public AI(Check color)
    {
        this.color = color;
		frameCenter = null;
    }

	/// <summary>
	/// Starts AlphaBeta algorithm to determine next move of AI.
	/// </summary>
    public void NextMove( )
    {
        //nextMove = new Coordinates(2, 2);
        //var bb = new Board(boardState);
        //boardState.MakeMove(5, 5, Check.Black);
        ////boardState.CopyState(bb);

        depth = 6;
        nextMove = null;
        cancel = false;
        var boardBefore = new Board(boardState);
        Thread t = new Thread(new ThreadStart(BestMoveLastMoveNeighbourhood));
        t.Priority = System.Threading.ThreadPriority.Highest;
        t.Start();
        var timeOut = 10000;
        while (t.IsAlive && timeOut > 0)
        {
            t.Join(500);
            timeOut -= 500;
        }
        if (t.IsAlive)
        {
            //t.Abort();
            cancel = true;
            t.Join();
            nextMove = null;
            boardState.CopyState(boardBefore);
        }
        if (nextMove == null)
        {
            cancel = false;
            depth = 2;
            BestMoveLastMoveNeighbourhood();
        }

        //int bestRating;
        //neighborhoodDistance = 2;
        //depth = 7;
        //nextMove = BestMoveForFrameMethod(out bestRating);

        //		frameCenter = boardState.GetLastMove ();
        //		neighborhoodDistance = 2;
        //		Coordinates move;
        //		Alphabeta(boardState, 6, int.MinValue, int.MaxValue, true, out move);
        //		nextMove = move;
    }

    private void BestMoveLastMoveNeighbourhood()
	{
        SetStaticSetForFrame(boardState.GetLastMove());
        Coordinates move;
		int rating = Alphabeta(boardState, depth, int.MinValue, int.MaxValue, true, out move);
        nextMove = move;
	}

    private void SetStaticSetForFrame(Coordinates frameCenter)
    {
        // search for checks in neighbourhood of last move neighboring opponents checks
        int neighbRadius = 4;
        staticSet = new List<Coordinates>();
        for (int i = frameCenter.x - neighbRadius; i < frameCenter.x + neighbRadius; i++)
        {
            for (int j = frameCenter.y - neighbRadius; j < frameCenter.y + neighbRadius; j++)
            {
                var coord = new Coordinates(i, j);
                if (!boardState.OutOfBoundaries(coord))
                {
                    if (boardState.GetCheck(coord) == Check.None)
                    {
                        if (boardState.AtLeastXNeighborsOfColor(coord, 1, Board.OtherPlayer(color)))
                        {
                            staticSet.Add(coord);
                        }
                    }

                }
            }
        }
    }

	private Coordinates BestMoveForFrameMethod(out int bestRating)
	{
		bestRating = int.MinValue;
		var threatRating = int.MaxValue;
		Coordinates threatFrame = null;
		Coordinates bestMove = null;
		Coordinates lastMove = boardState.GetLastMove ();
		for (int i = lastMove.x - 1; i < lastMove.x + 1; i++) {
			for (int j = lastMove.y - 1; j < lastMove.y + 1; j++) {
				if (boardState.GetCheck(i, j) != Check.Prohibited)
				{
					frameCenter = new Coordinates(i, j);
					Coordinates move;
                    SetStaticSetForFrame(frameCenter);
					int rating = Alphabeta(boardState, depth, int.MinValue, int.MaxValue, true, out move);
					if (move != null)
					if (rating > bestRating)
					{
						bestRating = rating;
						bestMove = move;
					}
                    SetStaticSetForFrame(frameCenter);
					rating = Alphabeta(boardState, depth, int.MinValue, int.MaxValue, false, out move);
					if (rating < threatRating)
					{
						threatRating = rating;
						threatFrame = frameCenter;
					}
				}
			}
		}
		int currentStatus = Heuristic (boardState);
		bestRating -= currentStatus;
		//threatRating++;
		threatRating -= currentStatus;
		if (threatRating + bestRating <= 0) {
			// Defense
			frameCenter = threatFrame;
			Coordinates move;
            SetStaticSetForFrame(frameCenter);
			int rating = Alphabeta(boardState, depth, int.MinValue, int.MaxValue, true, out move);
			bestMove = move;
		}
		frameCenter = null;
		return bestMove;
	}

	/// <summary>
	/// Minimax algorithm with AlphaBeta pruning.
	/// </summary>
	/// <param name="node">Node.</param>
	/// <param name="depth">Depth.</param>
	/// <param name="alpha">Alpha.</param>
	/// <param name="beta">Beta.</param>
	/// <param name="maximizingPlayer">Maximizing player.</param>
	/// <param name="move">Chosen move.</param>
    private int Alphabeta(Board node, int depth, int alpha, int beta, bool maximizingPlayer, out Coordinates move)
    {
		move = null;
        if (cancel) return 0;
        Coordinates m;
        if (depth == 0) // or node is a terminal node
        {
            return Heuristic(node);
        }
		var possibleMoves = PossibleMoves (node);
		if (possibleMoves.Count == 0) {
			return Heuristic(node);
		}
        if (maximizingPlayer)
        {
            int v = int.MinValue;
			foreach (var child in possibleMoves)
            {
				node.MakeMove(child.x, child.y, color);
                int ab = Alphabeta(node, depth - 1, alpha, beta, false, out m);
				node.GoOneMoveBack();
				if (ab > v)
				{
					v = ab;
					move = child;
				}
                alpha = Math.Max(alpha, v);
                if (beta <= alpha)
                    break; // β cut - off
            }
            return v;
        }
        else
        {
            int v = int.MaxValue;
            foreach (var child in possibleMoves)
            {
				node.MakeMove(child.x, child.y, Board.OtherPlayer(color));
                int ab = Alphabeta(node, depth - 1, alpha, beta, true, out m);
				node.GoOneMoveBack();
				if (ab < v)
				{
					v = ab;
					move = child;
				}
                beta = Math.Min(beta, v);
                if (beta <= alpha)
                    break; // α cut - off
            }
            return v;
        }
    }

	/// <summary>
	/// Heuristic which tells how well a player gets along.
	/// Returns difference between number of players dots.
	/// </summary>
	/// <param name="board">Board.</param>
    int Heuristic(Board board)
    {
        if (color == Check.White)
        {
            return board.numChecks[(int)Check.White] - board.numChecks[(int)Check.Black];
        }
        else
        {
            return board.numChecks[(int)Check.Black] - board.numChecks[(int)Check.White];
        }
    }

	/// <summary>
	/// Determines the next player on move.
	/// </summary>
	/// <returns>The player.</returns>
	/// <param name="board">Board.</param>
	Check NextPlayer(Board board)
	{
		if (board.GetCheck(board.GetLastMove().x, board.GetLastMove().y) == Check.Black)
		{
			return Check.White;
		}
		else
		{
			return Check.Black;
		}
	}

	/// <summary>
	/// Returns possible moves for player.
	/// It returns such a checks whih are neighbors of some stones of other player.
	/// If none such checks exists, all empty checks are returned.
	/// </summary>
	/// <returns>The possible moves.</returns>
	/// <param name="boardState">Board state.</param>
	HashSet<Coordinates> PossibleMoves(Board boardState)
	{
		var possibleMoves = new HashSet<Coordinates> ();

		if (staticSet != null) {
			foreach (var c in staticSet)
			{
				if (boardState.GetCheck(c) == Check.None)
				{
					possibleMoves.Add(c);
				}
			}
			return possibleMoves;
		}

		if (frameCenter != null) {
			var coordinates = boardState.GetAllNeighborsInDistance(frameCenter, neighborhoodDistance);
			foreach (var c in coordinates)
			{
				if (boardState.GetCheck(c.x, c.y) == Check.None)
				{
					possibleMoves.Add (c);
				}
			}
			if (boardState.GetCheck(frameCenter.x, frameCenter.y) == Check.None)
			{
				possibleMoves.Add(frameCenter);
			}
			if (possibleMoves.Count > 0)
				return possibleMoves;
		}

//		for (int i = 0; i < boardState.SizeX; i++) {
//			for (int j = 0; j < boardState.SizeY; j++) {
//				if (boardState.GetCheck(i, j) == Check.None)
//				{
//					var coord = new Coordinates(i, j);
//					var lastPlayer = boardState.GetCheck(boardState.GetLastMove());
//					if (boardState.AtLeastXNeighborsOfColor(coord, 1, lastPlayer))
//					{
//						possibleMoves.Add(coord);
//					}
//				}
//			}
//		}
		
		if (possibleMoves.Count == 0) {
			for (int i = 0; i < boardState.SizeX; i++) {
				for (int j = 0; j < boardState.SizeY; j++) {
					if (boardState.GetCheck(i, j) == Check.None)
					{
						var coord = new Coordinates(i, j);
						possibleMoves.Add(coord);
					}
				}
			}
		}

		return possibleMoves;
	}

	List<List<Coordinates>> TightlyCoupledGroups()
	{
		for (int i = 0; i < boardState.SizeX; i++) {
			for (int j = 0; j < boardState.SizeY; j++) {
				if (boardState.GetCheck(i, j) == Check.None)
				{
				}
			}
		}
		return null;
	}
}
