﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static Board board;
        static Coordinates cursor;
        static AI ai;

        static void PrintBoard()
        {
            Console.Clear();
            for (int y = 0; y < board.SizeY; y++)
            {
                for (int x = 0; x < board.SizeX; x++)
                {
                    if (x == cursor.x && y == cursor.y)
                    {
                        Console.Write("-");
                    }
                    else
                    {
                        switch (board.GetCheck(x, y))
                        {
                            case Check.Black:
                                Console.Write("x");
                                break;
                            case Check.White:
                                Console.Write("i");
                                break;
                            case Check.None:
                                Console.Write(" ");
                                break;
                            case Check.Prohibited:
                                Console.Write(".");
                                break;
                        }
                    }
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            board = new Board(25, 25);
            cursor = new Coordinates(5, 5);
            ai = new AI(Check.Black);
            ai.boardState = board;
            while (!board.IsFilled())
            {
                PrintBoard();
                ConsoleKeyInfo pressedKey;
                do
                {
                    pressedKey = Console.ReadKey(true);
                    switch (pressedKey.Key)
                    {
                        case ConsoleKey.UpArrow:
                            cursor.y--;
                            break;
                        case ConsoleKey.DownArrow:
                            cursor.y++;
                            break;
                        case ConsoleKey.LeftArrow:
                            cursor.x--;
                            break;
                        case ConsoleKey.RightArrow:
                            cursor.x++;
                            break;
                    }
                    PrintBoard();
                }
                while (pressedKey.Key != ConsoleKey.Enter);
                board.MakeMove(cursor.x, cursor.y, Check.White);
                ai.NextMove();
                board.MakeMove(ai.nextMove.x, ai.nextMove.y, Check.Black);
                PrintBoard();
            }
            
        }
    }
}
