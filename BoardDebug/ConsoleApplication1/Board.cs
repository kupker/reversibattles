﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;


/// <summary>
/// Possible values for check on playing board.
/// </summary>
public enum Check { None = 0, Black, White, Prohibited }


/// <summary>
/// Coordinates.
/// </summary>
public class Coordinates : IEquatable<Coordinates>
{
    public int x;
    public int y;

    public Coordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Coordinates(Coordinates other)
    {
        x = other.x;
        y = other.y;
    }

    public bool Equals(Coordinates other)
    {
        return this.x == other.x && this.y == other.y;
    }
}


/// <summary>
/// Move record.
/// </summary>
public class MoveRecord
{
	public enum Type { NormalMove, Occupy, SeedFillStart }

	public Coordinates coordinates;
	public Type type;
	public Check check;
	public Check originalCheck;

	public MoveRecord(Coordinates coordinates, Type type, Check check, Check originalCheck)
	{
		this.coordinates = coordinates;
		this.type = type;
		this.check = check;
		this.originalCheck = originalCheck;
	}

    public MoveRecord(Type type)
    {
        this.type = type;
    }
}


/// <summary>
/// Playing board.
/// </summary>
public class Board
{
    Check[,] board;
    int sizeX;
    int sizeY;

	// Number of each players checks (heuristic)
    public int[] numChecks;
	
	List<MoveRecord> moveHistory;

    Check[,] occupyArray;

	/// <summary>
	/// Initializes a new instance of the <see cref="Board"/> class.
	/// </summary>
	/// <param name="sizeX">Size x of board.</param>
	/// <param name="sizeY">Size y of board.</param>
	/// <param name="prohibited">If true sets all checks on board as prohibited</param>
    public Board(int sizeX, int sizeY, bool prohibited = false)
    {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        board = new Check[sizeX, sizeY];

		// Init board with default checks - none or prohibited
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
				if (prohibited)
				{
					board[i, j] = Check.Prohibited;
				}
				else
				{
					board[i, j] = Check.None;
				}
            }
        }

		SetBorder ();

		// Init number of each players checks
		// numChecks[0] will never be used, it is done in this way,
		// so it can be used like this: numChecks[(int)stoneColor]
		numChecks = new int[3];
		for (int i = 0; i < 3; i++)
		{
			numChecks[i] = 0;
		}

		moveHistory = new List<MoveRecord> ();

        occupyArray = new Check[sizeX, sizeY];
    }

    public Board(Board other)
    {
        sizeX = other.sizeX;
        sizeY = other.sizeY;

        board = new Check[sizeX, sizeY];

        // Init board with default checks - none or prohibited
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                board[i, j] = other.board[i, j];
            }
        }

        // Init number of each players checks
        // numChecks[0] will never be used, it is done in this way,
        // so it can be used like this: numChecks[(int)stoneColor]
        numChecks = new int[3];
        for (int i = 0; i < 3; i++)
        {
            numChecks[i] = other.numChecks[i];
        }

        moveHistory = new List<MoveRecord>();
        foreach (var move in other.moveHistory)
        {
            moveHistory.Add(move);
        }

        occupyArray = new Check[sizeX, sizeY];
    }

    public void CopyState(Board other)
    {
        sizeX = other.sizeX;
        sizeY = other.sizeY;

        // Init board with default checks - none or prohibited
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                board[i, j] = other.board[i, j];
            }
        }

        // Init number of each players checks
        // numChecks[0] will never be used, it is done in this way,
        // so it can be used like this: numChecks[(int)stoneColor]
        for (int i = 0; i < 3; i++)
        {
            numChecks[i] = other.numChecks[i];
        }

        moveHistory.Clear();
        foreach (var move in other.moveHistory)
        {
            moveHistory.Add(move);
        }
    }

    /// <summary>
    /// Sets the border of board as prohibited.
    /// </summary>
    public void SetBorder()
	{
		for (int i = 0; i < sizeX; i++)
		{
			board[i, 0] = Check.Prohibited;
			board[i, sizeY - 1] = Check.Prohibited;
		}
		for (int i = 0; i < sizeY; i++)
		{
			board[0, i] = Check.Prohibited;
			board[sizeX - 1, i] = Check.Prohibited;
		}
	}

	/// <summary>
	/// Sets all prohibited checks as empty except for border.
	/// </summary>
	public void EnableAllProhibited()
	{
		for (int i = 0; i < sizeX; i++)
		{
			for (int j = 0; j < sizeY; j++)
			{
				if (board[i, j] == Check.Prohibited)
					board[i, j] = Check.None;
			}
		}

		SetBorder ();
	}

	/// <summary>
	/// Determines whether the board is fully filled.
	/// </summary>
	/// <returns><c>true</c> if there is no more empty check; otherwise, <c>false</c>.</returns>
	public bool IsFilled()
	{
		for (int i = 0; i < sizeX; i++)
		{
			for (int j = 0; j < sizeY; j++)
			{
				if (board[i, j] == Check.None)
					return false;
			}
		}
		return true;
	}

	/// <summary>
	/// Gets the last move.
	/// </summary>
	/// <returns>The last move.</returns>
    public Coordinates GetLastMove()
    {
        return moveHistory.Last().coordinates;
    }

	/// <summary>
	/// Gets the check.
	/// </summary>
	/// <returns>The check.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
    public Check GetCheck(int x, int y)
    {
        if (x < 0 || y < 0 || x >= sizeX || y >= sizeY)
        {
            return Check.None;
        }
        return board[x, y];
    }

	/// <summary>
	/// Gets the check.
	/// </summary>
	/// <returns>The check.</returns>
	/// <param name="coord">Coordinates.</param>
	public Check GetCheck(Coordinates coord)
	{
		return GetCheck(coord.x, coord.y);
	}

	/// <summary>
	/// Sets check on the board.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="value">Value.</param>
    public void SetBoard(int x, int y, Check value)
    {
        board[x, y] = value;
    }

	/// <summary>
	/// Players move - it sets the stone on board and if some territory was surrouded it is occupied.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="value">Stone color.</param>
    public void MakeMove(int x, int y, Check value, bool occupy = true)
    {
        var originalCheck = board[x, y];
        board[x, y] = value;
        numChecks[(int)value]++;
		moveHistory.Add (new MoveRecord(new Coordinates(x, y), MoveRecord.Type.NormalMove, value, originalCheck));
        if (occupy)
            Occupy(new Coordinates(x, y));
    }

	/// <summary>
	/// Reverts last move.
	/// </summary>
	public void GoOneMoveBack()
	{
		while (moveHistory.Last().type == MoveRecord.Type.Occupy) {
			var lastMove = moveHistory.Last();
			moveHistory.Remove(lastMove);
			board [lastMove.coordinates.x, lastMove.coordinates.y] = lastMove.originalCheck;
			numChecks [(int)lastMove.check]--;
			if (lastMove.originalCheck != Check.None)
			{
				numChecks [(int)lastMove.originalCheck]++;
			}
		}
		if (moveHistory.Last().type == MoveRecord.Type.NormalMove) {
			var lastMove = moveHistory.Last();
			moveHistory.Remove(lastMove);
			board [lastMove.coordinates.x, lastMove.coordinates.y] = lastMove.originalCheck;
			numChecks [(int)lastMove.check]--;
		}
	}

	/// <summary>
	/// Gets the size x.
	/// </summary>
	/// <value>The size x.</value>
    public int SizeX
    {
        get
        {
            return sizeX;
        }
    }

	/// <summary>
	/// Gets the size y.
	/// </summary>
	/// <value>The size y.</value>
    public int SizeY
    {
        get
        {
            return sizeY;
        }
    }

	/// <summary>
	/// Returns coordinates of checks above, bellow, right and left the check.
	/// </summary>
	/// <returns>The neigbors.</returns>
	/// <param name="check">Check.</param>
    List<Coordinates> GetNeigbors(Coordinates check)
    {
        var result = new List<Coordinates>();
        result.Add(new Coordinates(check.x - 1, check.y));
        result.Add(new Coordinates(check.x + 1, check.y));
        result.Add(new Coordinates(check.x, check.y - 1));
        result.Add(new Coordinates(check.x, check.y + 1));
        return result;
    }

	/// <summary>
	/// Returns coordinates of all adjoining checks.
	/// </summary>
	/// <returns>The all neighbors.</returns>
	/// <param name="check">Check.</param>
    public static List<Coordinates> GetAllNeighbors(Coordinates check)
    {
        var result = new List<Coordinates>();
        for (int i = check.x - 1; i <= check.x + 1; i++)
        {
            for (int j = check.y - 1; j <= check.y + 1; j++)
            {
                if (i != check.x || j != check.y)
                {
                    result.Add(new Coordinates(i, j));
                }
            }
        }
        return result;
    }

	public List<Coordinates> GetAllNeighborsInDistance(Coordinates check, int distance)
	{
		var result = new List<Coordinates>();
		for (int i = check.x - distance; i <= check.x + distance; i++)
		{
			for (int j = check.y - distance; j <= check.y + distance; j++)
			{
				if (i != check.x || j != check.y)
				{
					if (i >= 0 && j >= 0 && i < sizeX && j < sizeY)
					{
						result.Add(new Coordinates(i, j));
					}
				}
			}
		}
		return result;
	}

	/// <summary>
	/// Returns array representing board only with stones of player.
	/// </summary>
	/// <returns>Board with players dots.</returns>
	/// <param name="player">Player.</param>
    Check[,] MarkPlayersDots(Check player)
    {
        //var array = new Check[sizeX, sizeY];
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                if (board[i, j] == player)
                {
                    occupyArray[i, j] = Check.White;
                }
                else
                {
                    occupyArray[i, j] = Check.None;
                }
            }
        }
        return occupyArray;
    }

	/// <summary>
	/// Other players color.
	/// </summary>
	/// <returns>The player.</returns>
	/// <param name="player">Player.</param>
    public static Check OtherPlayer(Check player)
    {
        if (player == Check.Black)
            return Check.White;
        else
            return Check.Black;
    }

	/// <summary>
	/// Returns true if check neighbors with at least x neigbors of color.
	/// </summary>
	/// <returns>The least X neighbors of color.</returns>
	/// <param name="check">Check.</param>
	/// <param name="x">The x coordinate.</param>
	/// <param name="color">Color.</param>
    public bool AtLeastXNeighborsOfColor(Coordinates check, int x, Check color)
    {
        int i = 0;
        var adjoining = GetAllNeighbors(check);
        foreach (var a in adjoining)
        {
            if (board[a.x, a.y] == color)
            {
                i++;
                if (i >= x)
                {
                    return true;
                }
            }
        }
        return false;
    }

	/// <summary>
	/// If some territory was surrounded occupy it.
	/// </summary>
	/// <param name="seed">Coordinates of last move.</param>
    void Occupy(Coordinates seed)
    {
		if (!AtLeastXNeighborsOfColor(seed, 2, board[seed.x, seed.y]))
        {
            return;
        }

        // Using seed filling algorithm determine if some territory was surronded.
		// For every neigboring empty check start seed filling algorithm and
		// if it do not hit board boundaries it means that territory was surrounded.
        var playerColor = board[seed.x, seed.y];
        var adjoining = GetAllNeighbors(seed);
        List<List<Coordinates>> filling = new List<List<Coordinates>>();
        foreach (var a in adjoining)
        {
            //var array = MarkPlayersDots(playerColor);
            
            if (board[a.x, a.y] != playerColor)
            {
                var zarazka = new MoveRecord(MoveRecord.Type.SeedFillStart);
                moveHistory.Add(zarazka);

                var hitBoundaries = false;
                var fill = SeedFill(a, playerColor, out hitBoundaries);

                //revert
                while (moveHistory.Last() != zarazka)
                {
                    GoOneMoveBack();
                }
                moveHistory.Remove(zarazka);

                if (!hitBoundaries)
                {
                    filling.Add(fill);
                }
            }

        }

        // Fill the surrounded terrritory with stones.
        foreach (var x in filling)
        {
            foreach (var f in x)
            {
                if (board[f.x, f.y] == Check.None)
                    numChecks[(int)playerColor]++;
                else if (board[f.x, f.y] != playerColor)
                {
                    numChecks[(int)playerColor]++;
                    numChecks[(int)OtherPlayer(playerColor)]--;
                }

				moveHistory.Add(new MoveRecord(f, MoveRecord.Type.Occupy, playerColor, board[f.x, f.y]));
                board[f.x, f.y] = playerColor;
            }
        }
    }

	/// <summary>
	/// Seed filling algorithm.
	/// </summary>
	/// <returns>Retuns filled territory.</returns>
	/// <param name="array">Board with players stones.</param>
	/// <param name="check">Coordinate of seed.</param>
	/// <param name="hitBoundaries">Hit boundaries.</param>
    List<Coordinates> SeedFill(Coordinates check, Check playerColor, out bool hitBoundaries)
    {
        var result = new List<Coordinates>();
        hitBoundaries = false;
        //array[check.x, check.y] = Check.White;
        MakeMove(check.x, check.y, playerColor, false);
        result.Add(check);
        var adjoining = GetNeigbors(check);
        foreach (var a in adjoining)
        {
            if (OutOfBoundaries(a))
            {
                hitBoundaries = true;
                return result;
            }
            if (board[a.x, a.y] != playerColor)
            {
                result.AddRange(SeedFill(a, playerColor, out hitBoundaries));
                if (hitBoundaries)
                {
                    return result;
                }
            }
        }
        return result;
    }

	/// <summary>
	/// Returns true if coordinates are out of board.
	/// </summary>
	/// <param name="check">Coordinates.</param>
    public bool OutOfBoundaries(Coordinates check)
    {
        return check.x < 0 || check.x >= sizeX || check.y < 0 || check.y >= sizeY;
    }
}
