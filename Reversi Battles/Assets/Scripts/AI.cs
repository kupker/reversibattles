using System;
using System.Collections.Generic;
using System.Threading;

/// <summary>
/// AI using AlphaBeta algorithm.
/// </summary>
public class AI
{
    Check.State color;    //color of AI
    bool easy;

    int depth;  //depth of alfaBeta
    int neighbRadius;   //how far from lastMove look for possible moves
    List<Coordinates> staticSet; //represents checks in neighborhood of lastMove, it does not change during alfaBeta

    bool threadCancel;

    // Board. Should be set externaly before NextMove is called.
    // Reason that this is not a parameter of NextMove is that
    // in this way, NextMove can be called in another thread.
    public Board boardState;

    // Next move of AI. This should be checked after NextMove is called.
    // This is not return value of NextMove for the same reason as above.
    public Coordinates nextMove;

    /// <summary>
    /// Initializes a new instance of the <see cref="AI"/> class.
    /// </summary>
    /// <param name="color">Color of stones of AIs.</param>
    public AI(Check.State color, bool easy)
    {
        this.color = color;
        this.easy = easy;
    }

    /// <summary>
    /// Starts AlphaBeta algorithm to determine next move of AI.
    /// First of all alphaBeta with bigger depth is called in another thread.
    /// If it times out, alphaBeta with smaller depth is called.
    /// </summary>
    public void NextMove()
    {
        depth = 6;
        neighbRadius = 4;
        nextMove = null;
        threadCancel = false;
        var boardBefore = new Board(boardState);

        if (!easy)
        {
            // start new thread with alfaBeta with bigger depth
            Thread t = new Thread(new ThreadStart(BestMoveLastMoveNeighbourhood));
            t.Priority = System.Threading.ThreadPriority.Highest;
            t.Start();

            var timeOut = 10000;    // how long to wait for alfaBeta with bigger depth
            while (t.IsAlive && timeOut > 0)
            {
                t.Join(500);
                timeOut -= 500;
            }
            if (t.IsAlive)
            {   // timed out
                threadCancel = true;
                t.Join();
                nextMove = null;
                boardState.CopyState(boardBefore);
            }
        }

        // If previous effort timed out, use alphBeta with smaller depth
        if (nextMove == null)
        {
            threadCancel = false;
            depth = 2;
            BestMoveLastMoveNeighbourhood();
        }

		if (nextMove == null)
		{
			AnyPossibleMove ();
		}
    }

	/// <summary>
	/// Picks any possible move
	/// </summary>
	private void AnyPossibleMove()
	{
		for (int i = 0; i < boardState.SizeX; i++)
		{
			for (int j = 0; j < boardState.SizeY; j++)
			{
				if (boardState.GetCheck (i, j).state == Check.State.None)
				{
					nextMove = new Coordinates (i, j);
					break;
				}
			}
			if (nextMove != null)
				break;
		}
	}

    /// <summary>
    /// Sets static set and starts alfaBeta
    /// </summary>
    private void BestMoveLastMoveNeighbourhood()
    {
        SetStaticSetForFrame(boardState.GetLastMove());
        Coordinates move;
        Alphabeta(boardState, depth, int.MinValue, int.MaxValue, true, out move);
        nextMove = move;
    }

    /// <summary>
    /// Searches for checks in neighbourhood of last move neighboring opponents checks.
    /// If such a static set would not be used, everytime alfaBeta would ask for possible moves,
    /// it would give more possible moves, that it is needed.
    /// </summary>
    /// <param name="frameCenter"></param>
    private void SetStaticSetForFrame(Coordinates frameCenter)
    {
        staticSet = new List<Coordinates>();
        for (int i = frameCenter.x - neighbRadius; i < frameCenter.x + neighbRadius; i++)
        {
            for (int j = frameCenter.y - neighbRadius; j < frameCenter.y + neighbRadius; j++)
            {
                var coord = new Coordinates(i, j);
                if (!boardState.OutOfBoundaries(coord) &&
                    boardState.GetCheck(coord).state == Check.State.None &&
                    boardState.AtLeastXNeighborsOfColor(coord, 1, Board.OtherPlayer(color)))
                {
                    staticSet.Add(coord);
                }
            }
        }
    }

    /// <summary>
    /// Minimax algorithm with AlphaBeta pruning.
    /// </summary>
    /// <param name="node">Node.</param>
    /// <param name="depth">Depth.</param>
    /// <param name="alpha">Alpha.</param>
    /// <param name="beta">Beta.</param>
    /// <param name="maximizingPlayer">Maximizing player.</param>
    /// <param name="move">Chosen move.</param>
    private int Alphabeta(Board node, int depth, int alpha, int beta, bool maximizingPlayer, out Coordinates move)
    {
        move = null;
        if (threadCancel) return 0;
        Coordinates m;
        if (depth == 0) // or node is a terminal node
        {
            return Heuristic(node);
        }
        var possibleMoves = PossibleMoves(node);
        if (possibleMoves.Count == 0)
        {
            return Heuristic(node);
        }
        if (maximizingPlayer)
        {
            int v = int.MinValue;
            foreach (var child in possibleMoves)
            {
                node.MakeMove(child.x, child.y, color);
                int ab = Alphabeta(node, depth - 1, alpha, beta, false, out m);
                node.GoOneMoveBack();
                if (ab > v)
                {
                    v = ab;
                    move = child;
                }
                alpha = Math.Max(alpha, v);
                if (beta <= alpha)
                    break; // β cut - off
            }
            return v;
        }
        else
        {
            int v = int.MaxValue;
            foreach (var child in possibleMoves)
            {
                node.MakeMove(child.x, child.y, Board.OtherPlayer(color));
                int ab = Alphabeta(node, depth - 1, alpha, beta, true, out m);
                node.GoOneMoveBack();
                if (ab < v)
                {
                    v = ab;
                    move = child;
                }
                beta = Math.Min(beta, v);
                if (beta <= alpha)
                    break; // α cut - off
            }
            return v;
        }
    }

    /// <summary>
    /// Heuristic which tells how well a player gets along.
    /// Returns difference between number of players dots.
    /// </summary>
    /// <param name="board">Board.</param>
    int Heuristic(Board board)
    {
        if (color == Check.State.White)
        {
            return board.numChecks[(int)Check.State.White] - board.numChecks[(int)Check.State.Black];
        }
        else
        {
            return board.numChecks[(int)Check.State.Black] - board.numChecks[(int)Check.State.White];
        }
    }

    /// <summary>
    /// Determines the next player on move.
    /// </summary>
    /// <returns>The player.</returns>
    /// <param name="board">Board.</param>
    Check.State NextPlayer(Board board)
    {
        if (board.GetCheck(board.GetLastMove().x, board.GetLastMove().y).state == Check.State.Black)
        {
            return Check.State.White;
        }
        else
        {
            return Check.State.Black;
        }
    }

    /// <summary>
    /// Returns possible moves for player.
    /// It returns such a checks from staticSet which are neighbors of stones of other player.
    /// If none such checks exists, all empty checks are returned.
    /// </summary>
    /// <returns>The possible moves.</returns>
    /// <param name="boardState">Board state.</param>
    HashSet<Coordinates> PossibleMoves(Board boardState)
    {
        var possibleMoves = new HashSet<Coordinates>();

        if (staticSet != null)
        {
            foreach (var c in staticSet)
            {
                if (boardState.GetCheck(c).state == Check.State.None)
                {
                    possibleMoves.Add(c);
                }
            }
            return possibleMoves;
        }

        if (possibleMoves.Count == 0)
        {
            for (int i = 0; i < boardState.SizeX; i++)
            {
                for (int j = 0; j < boardState.SizeY; j++)
                {
                    if (boardState.GetCheck(i, j).state == Check.State.None)
                    {
                        var coord = new Coordinates(i, j);
                        possibleMoves.Add(coord);
                    }
                }
            }
        }

        return possibleMoves;
    }
}
