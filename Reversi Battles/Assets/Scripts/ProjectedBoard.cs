using UnityEngine;
using System.Collections.Generic;

public class ProjectedBoard : MonoBehaviour
{
    public Dot[] dots;
    public Triangle[] triangles;
    public GameObject Stone;//prefab
    public GameObject Circle;//prefab
    public GameObject LinePrafab;
    public GameObject TrianglePrefab;
    private int width, height; // horizontal/vertical dots count
    private float dotSize;
    public float boardScale;

    public Material mat;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //drawCircles();
    }

    public void GenerateDots(Board board)
    {
        //Debug.Log("GenerateDots");
        width = board.SizeX;
        height = board.SizeY;
        dotSize = boardScale / width;
        dots = new Dot[width * height];
        var offsetX = -dotSize * width / 2;
        var offsetY = -dotSize * height / 2;
        int generatedId = 0;
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                var x = offsetX + i * dotSize;
                var y = offsetY + j * dotSize;

                //Debug.Log("instantiate: x=" + i + " y=" + j);
                var dot = ((GameObject)Instantiate(Stone, new Vector3(x, y), Quaternion.identity)).GetComponent<Dot>();
                dot.transform.SetParent(transform);
                dot.coordX = i;
                dot.coordY = j;
                dot.id = generatedId++;
                dots[j * width + i] = dot;

				if (board.GetCheck (i, j).state == Check.State.Prohibited) {
					dot.GetComponent<MeshRenderer> ().enabled = false;
				}
            }
        }

        ProhibitHiddenDots(board);

        generateTriangles();
        createGrid();

		//transform.position = new Vector3 (1, 1, 0);
		centerX();
		centerY ();
    }

    public void ProhibitHiddenDots(Board board)
    {
        for (int j = 0; j < board.SizeY; j++)
        {
            for (int i = 0; i < board.SizeX; i++)
            {
                var ray = new Ray(dots[j * width + i].transform.position, new Vector3(0, 0, -10));
                var hit = Physics2D.GetRayIntersection(ray);
                if (hit.collider != null)
                {
                    board.SetBoard(i, j, Check.State.Prohibited);
                }
            }
        }
    }

    public Dot findDot(int id)
    {
        foreach (Dot dot in dots)
        {
            if (dot.id == id)
            {
                return dot;
            }
        }

        return null;
    }

    public Dot findDot(int x, int y)
    {
        return dots[y * width + x];
    }

    public void createCircle(Path circle)
    {
        if (circle == null)
        {
            return;
        }

        Dot dotA, dotB;
        Line line;
        Color circleColor = findDot(circle.vertices[0]).check == Check.State.Black ? Color.red : Color.blue;

        for (int i = 0; i < circle.vertices.Count; i++)
        {
            dotA = findDot(circle.vertices[i]);
            dotB = findDot(circle.vertices[(i + 1) % circle.vertices.Count]);
            line = ((GameObject)Instantiate(LinePrafab, new Vector3(0, 0, 0), Quaternion.identity)).GetComponent<Line>();
			line.transform.SetParent(this.transform);
			line.initialize(new Vector3(dotA.transform.localPosition.x, dotA.transform.localPosition.y, -0.02f), new Vector3(dotB.transform.localPosition.x, dotB.transform.localPosition.y, -0.02f), circleColor, 2);
        }
    }

    public void generateTriangles()
    {
        triangles = new Triangle[(width - 1) * (height - 1) * 4];
        float offsetX = -dotSize * width / 2;
        float offsetY = -dotSize * height / 2;
        int triangleId = 0;

        for (int j = 1; j < height; j++)
        {
            for (int i = 0; i < (width - 1); i++)
            {
                for (int t = 0; t < 4; t++)
                {
                    createTriangle(4 * triangleId + t, t, offsetX + i * dotSize, offsetY + j * dotSize);
                }
                triangleId++;
            }
        }
    }

    public void createTriangle(int id, int type, float x, float y)
    {
        triangles[id] = ((GameObject)Instantiate(TrianglePrefab, new Vector3(x + dotSize / 2.0f, y - dotSize / 2.0f, 0.02f), Quaternion.identity)).GetComponent<Triangle>();
		triangles [id].transform.SetParent (this.transform);      
		triangles[id].transform.eulerAngles = new Vector3(0, 0, -90 * type);
        triangles[id].transform.localScale = new Vector3(dotSize / 2.0f, dotSize / 2.0f, 0.02f);
        triangles[id].id = id;
    }

    public void changeTrianglesOwner(List<int> triangleIDs, Check.State owner)
    {
        foreach (int triangleId in triangleIDs)
        {
            triangles[triangleId].setOwner(owner);
        }
    }

    public void fillArea(int dotId, Check.State owner)
    {
        List<int> neighbourTriangles = findDot(dotId).getNeighbourTriangles(width, height);
        List<int> trianglesToBeColored = new List<int>();

        for (int i = 0; i < neighbourTriangles.Count; i++)
        {
            if (triangles[neighbourTriangles[i]].owner != owner)
            {
                if (getTrianglesToBeColored(neighbourTriangles[i], owner, ref trianglesToBeColored))
                {
                    changeTrianglesOwner(trianglesToBeColored, owner);
                }

                trianglesToBeColored.Clear();
            }
        }
    }

    public bool getTrianglesToBeColored(int triangleId, Check.State owner, ref List<int> output)
    {
        Triangle triangle = triangles[triangleId];

        if (triangle.isBorder(width, height))
        {
            return false;
        }
        //triangle.setOwner(owner);
        output.Add(triangleId);
        Vector2 boardSize = new Vector2(width - 1, height - 1);
        //Debug.Log(id + " " + getNeighbour(1, boardSize) + " " + getNeighbour(3, boardSize) + " " + getNeighbour(-2, boardSize));
        //Debug.Log(id + " " + getNeighbourDot(0, boardSize) + " " + getNeighbourDot(1, boardSize) + " " + getNeighbourDot(2, boardSize) + " " + getNeighbourDot(3, boardSize));

        if (((triangle.id % 4 == 3 || triangle.id % 4 == 1) && !(dots[triangle.getNeighbourDot(0, boardSize)].check == owner && dots[triangle.getNeighbourDot(2, boardSize)].check == owner)) ||
            ((triangle.id % 4 == 2 || triangle.id % 4 == 0) && !(dots[triangle.getNeighbourDot(1, boardSize)].check == owner && dots[triangle.getNeighbourDot(3, boardSize)].check == owner)) ||
            (dots[triangle.getNeighbourDot(0, boardSize)].check == owner) && dots[triangle.getNeighbourDot(1, boardSize)].check == owner && dots[triangle.getNeighbourDot(2, boardSize)].check == owner && dots[triangle.getNeighbourDot(3, boardSize)].check == owner)
        {
            if (!output.Contains(triangle.getNeighbour(1, boardSize)))
            {
                //triangles[triangle.getNeighbour(1, boardSize)].setColor(new Color(1, 0, 0, 0.5f));
                if (!getTrianglesToBeColored(triangle.getNeighbour(1, boardSize), owner, ref output))
                {
                    return false;
                }
            }
        }

        if (((triangle.id % 4 == 3 || triangle.id % 4 == 1) && !(dots[triangle.getNeighbourDot(1, boardSize)].check == owner && dots[triangle.getNeighbourDot(3, boardSize)].check == owner)) ||
            ((triangle.id % 4 == 2 || triangle.id % 4 == 0) && !(dots[triangle.getNeighbourDot(0, boardSize)].check == owner && dots[triangle.getNeighbourDot(2, boardSize)].check == owner)) ||
            (dots[triangle.getNeighbourDot(0, boardSize)].check == owner) && dots[triangle.getNeighbourDot(1, boardSize)].check == owner && dots[triangle.getNeighbourDot(2, boardSize)].check == owner && dots[triangle.getNeighbourDot(3, boardSize)].check == owner)
        {
            if (!output.Contains(triangle.getNeighbour(3, boardSize)))
            {
                //triangles[triangle.getNeighbour(3, boardSize)].setColor(new Color(1, 0, 0, 0.5f));
                if (!getTrianglesToBeColored(triangle.getNeighbour(3, boardSize), owner, ref output))
                {
                    return false;
                }
            }
        }

        if (triangle.getNeighbour(-2, boardSize) >= 0)
        {
            if ((triangle.id % 4 == 3 && !(dots[triangle.getNeighbourDot(0, boardSize)].check == owner && dots[triangle.getNeighbourDot(1, boardSize)].check == owner)) ||
                (triangle.id % 4 == 2 && !(dots[triangle.getNeighbourDot(1, boardSize)].check == owner && dots[triangle.getNeighbourDot(2, boardSize)].check == owner)) ||
                (triangle.id % 4 == 1 && !(dots[triangle.getNeighbourDot(2, boardSize)].check == owner && dots[triangle.getNeighbourDot(3, boardSize)].check == owner)) ||
                (triangle.id % 4 == 0 && !(dots[triangle.getNeighbourDot(3, boardSize)].check == owner && dots[triangle.getNeighbourDot(0, boardSize)].check == owner)))
            {
                if (!output.Contains(triangle.getNeighbour(-2, boardSize)))
                {
                    //triangles[triangle.getNeighbour(-2, boardSize)].setColor(new Color(1, 0, 0, 0.5f));
                    if (!getTrianglesToBeColored(triangle.getNeighbour(-2, boardSize), owner, ref output))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

	private void centerX()
	{
		var offsetX = -dotSize * width / 2;
		var right = offsetX;
		var left = -offsetX - dotSize;

		var diff = Mathf.Abs (left - right);
		var offset = Mathf.Abs (left) - (diff / 2);
		transform.position = new Vector3 (-offset, transform.position.y, 0);
	}

	private void centerY()
	{
		var offsetY = -dotSize * height / 2;
		var up = offsetY;
		var down = -offsetY - dotSize;

		var diff = Mathf.Abs (down - up);
		var offset = Mathf.Abs (down) - (diff / 2);
		transform.position = new Vector3 (transform.position.x, -offset + 0.1f, 0);
	}

    private void createGrid()
    {
        var offsetX = -dotSize * width / 2;
        var offsetY = -dotSize * height / 2;
        Line line;

        for (int x = 0; x < width; x++)
        {
            line = ((GameObject)Instantiate(LinePrafab, new Vector3(0, 0, 0), Quaternion.identity)).GetComponent<Line>();
            line.transform.SetParent(this.transform);
            line.initialize(new Vector3(offsetX + x * dotSize, offsetY, 0), new Vector3(offsetX + x * dotSize, -offsetY - dotSize, 0), Color.gray, 2);
        }

        for (int y = 0; y < height; y++)
        {
            line = ((GameObject)Instantiate(LinePrafab, new Vector3(0, 0, 0), Quaternion.identity)).GetComponent<Line>();
            line.transform.SetParent(this.transform);
			line.initialize (new Vector3 (offsetX, offsetY + y * dotSize, 0), new Vector3 (-offsetX - dotSize, offsetY + y * dotSize, 0), Color.gray, 2);
        }
    }
}
