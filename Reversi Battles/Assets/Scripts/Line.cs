﻿using UnityEngine;
using System.Collections;

public class Line : MonoBehaviour
{
    public Vector3 start = new Vector3(0, 0, 0);
    public Vector3 end = new Vector3(1, 1, 1);
    public Color color = Color.blue;
    public uint size = 2;
    public Vector3 vector;

    // Use this for initialization
    void Start()
    {
        vector = new Vector3(end.x - start.x, end.y - start.y, end.z - start.z);
        transform.localScale = new Vector3(Mathf.Sqrt(Mathf.Pow(vector.x, 2) + Mathf.Pow(vector.y, 2)), 0.01f * size, 0.01f * size);
        transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(vector.y, vector.x) / Mathf.PI * 180);
		transform.localPosition = start + vector / 2;
        gameObject.GetComponent<MeshRenderer>().material.color = color;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void initialize(Vector3 start, Vector3 end, Color color, uint size)
    {
        this.start = start;
        this.end = end;
        this.color = color;
        this.size = size;
        Start();
    }
}
