using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{

    GameManager gameManager;
    GameObject canvas;

    public int state = 0;
    bool initialized = false;

    //float delayCounter = 0;

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        canvas = GameObject.Find("Canvas");
    }

    void Update()
    {
        if (!initialized)
        {
            gameManager.madeMoveCallback = new MadeMoveCallback(MadeMove);
            SetFirstStage();
            initialized = true;
            NextState();
        }

        if (ShowingText() && Input.GetMouseButtonDown(0))
        {
            NextState();
        }
    }

    bool ShowingText()
    {
        switch (state)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return true;
            default:
                return false;
        }
    }

    void NextState()
    {
        state++;
        //Debug.Log ("next state: " + state);
        switch (state)
        {
            case 1:
                SavedDataManager.updatePlayerProfile(SavedDataManager.currentProfile, SavedDataManager.playerProfiles[SavedDataManager.currentProfile].campaignProgress, 1, SavedDataManager.playerProfiles[SavedDataManager.currentProfile].stars, true);
                canvas.transform.Find("Text").GetComponent<Text>().text = "Vítej ve hře Dotky!";
                break;
            case 2:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Tvým úkolem bude umisťovat tečky a získávat území.";
                break;
            case 3:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Území můžeš získat oblíčením protivníkových teček.";
                break;
            case 4:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Tak si to vyzkoušíme.";
                break;
            case 5:
                canvas.SetActive(false);
                gameManager.isDisabled = false;
                break;
            case 6:
                break;
            case 7:
                canvas.SetActive(true);
                gameManager.isDisabled = true;
                canvas.transform.Find("Text").GetComponent<Text>().text = "Skvělé!";
                break;
            case 8:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Nebude to ale tak jednoduché. I protivník může umisťovat tečky.";
                break;
            case 9:
                canvas.SetActive(false);
                gameManager.isDisabled = false;
                var cam = GameObject.Find("Main Camera").GetComponent<CameraBahaviour>();
                cam.treshold = 2;
                cam.speed = 0.1f;
                gameManager.EnableAllProhibited();
                gameManager.updateGraphAndProjectedBoard();
                gameManager.switchPlayers = true;
                gameManager.isAiEnabled = true;
                gameManager.gameEnd = false;
                break;
            case 10:
                break;
            case 11:
                canvas.SetActive(true);
                gameManager.isDisabled = true;
                canvas.transform.Find("Text").GetComponent<Text>().text = "V dolní části obrazovky můžeš vidět aktuální skóre.";
                break;
            case 12:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Za každou tečku získáváš +1 bod.";
                break;
            case 13:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Obklíčením protivníka získáš +2 body za každou obklíčenou tečku. Počítají se i ty, které ještě nebyly zabrané!";
                break;
            case 14:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Ale pozor! Za každou ztracenou tečku se ti odečte -1 bod!";
                break;
            case 15:
                canvas.transform.Find("Text").GetComponent<Text>().text = "Dokážeš být lepší než protivník? ";
                break;
            case 16:
                canvas.SetActive(false);
                gameManager.isDisabled = false;
                gameManager.isAiEnabled = true;
                break;
            case 17:
                gameManager.isDisabled = true;
                Invoke("ShowPostGameWindow", 0.5f);
                break;
        }
    }

    void ShowPostGameWindow()
    {
        MainMenu mainMenu = GameObject.Find("MenuManager").GetComponent<MainMenu>();
        mainMenu.showPostGameWindow();
    }

    void SetFirstStage()
    {
        if (gameManager == null)
        {
            Debug.Log("gameManager is null");
        }
        else if (gameManager.board == null)
        {
            Debug.Log("gameManager.board is null");
        }

        gameManager.board.MakeMove(4, 4, Check.State.Black);
        gameManager.board.MakeMove(4, 5, Check.State.Black);
        gameManager.board.MakeMove(5, 6, Check.State.Black);
        gameManager.board.MakeMove(5, 7, Check.State.Black);

        gameManager.board.MakeMove(3, 5, Check.State.White);
        gameManager.board.MakeMove(5, 5, Check.State.White);
        gameManager.board.MakeMove(4, 6, Check.State.White);
        gameManager.board.MakeMove(6, 6, Check.State.White);
        gameManager.board.MakeMove(5, 8, Check.State.White);

        gameManager.board.MakeMove(4, 7, Check.State.None);
        gameManager.board.MakeMove(6, 7, Check.State.None);

        gameManager.updateGraphAndProjectedBoard();
        gameManager.projectedBoard.findDot(4, 7).Pulsating = true;
        gameManager.projectedBoard.findDot(6, 7).Pulsating = true;
    }

    public void MadeMove()
    {
        if (!ShowingText())
        {
            if (state == 5)
            {
                if (gameManager.board.IsFilled())
                {
                    NextState();
                }
            }
            else
            {
                if (gameManager.gameEnd)
                {
                    NextState();
                }
            }
            if (state == 9 && gameManager.board.numChecks[2] >= 15)
            {
                gameManager.isDisabled = true;
                NextState();
            }
        }
    }
}