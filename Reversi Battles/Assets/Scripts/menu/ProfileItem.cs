﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ProfileItem : MonoBehaviour, IPointerClickHandler 
{
    public int profileIndex;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject profileMenu = GameObject.Find("ProfileMenu");

        if (!profileMenu)
        {
            return;
        }

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (eventData.clickCount == 1)
            {
                MainMenu.menuManager.GetComponent<MainMenu>().selectPlayerProfile(profileIndex);
            }
            else
            {
                MainMenu.menuManager.GetComponent<MainMenu>().selectPlayerProfile(profileIndex);
                MainMenu.menuManager.GetComponent<MainMenu>().applySelectedProfile();
            }
        }
    }
}
