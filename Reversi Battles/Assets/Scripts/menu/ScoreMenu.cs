﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreMenu : MonoBehaviour
{
    private Text playerList, scoreList;
    // Use this for initialization
    void Start()
    {
        Transform[] childs = GetComponentsInChildren<Transform>();

        foreach (Transform child in childs)
        {
            if (child.name == "PlayerList")
            {
                playerList = child.GetComponent<Text>();
            }
            else if (child.name == "ScoreList")
            {
                scoreList = child.GetComponent<Text>();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void updateScoreTable()
    {
        playerList.text = "";
        scoreList.text = "";

        foreach (ScoreItem scoreItem in SavedDataManager.scoreTable)
        {
            playerList.text += scoreItem.playerName + "\r\n";
            scoreList.text += scoreItem.score + "\r\n";
        }
    }
}
