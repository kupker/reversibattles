﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class DropDown : MonoBehaviour, IPointerClickHandler, IPointerExitHandler {

    public RectTransform container;
    public bool isOpen;
    private int count = 1;


    // Use this for initialization
    void Start () {
        container = transform.FindChild("Container").GetComponent<RectTransform>();
        isOpen = false;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 scale = container.localScale;
        scale.y = Mathf.Lerp(scale.y, isOpen ? 1 : 0, Time.deltaTime * 12);
        container.localScale = scale;
	}




    public void OnPointerClick(PointerEventData eventData)
    {
        if (count == 1)
        {
            isOpen = true;
            count = 2;
        } else {
            isOpen = false;
            count = 1;
        }  
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOpen = false;
        count = 1;
    }
    /*
   public void OnPointerEnter(PointerEventData eventData)
{
   isOpen = true;
}
*/
}
