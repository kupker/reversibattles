﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelSelectionButtonScript : MonoBehaviour
{
    public void LoadLevel(int levelNumber)
    {
        SceneManager.LoadScene(levelNumber);
    }
}
