﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class Level5Manager : LevelManager
{

	protected override void SetFirstStage()
	{
		gameManager.board.MakeMove(5, 5, Check.State.Black);
		gameManager.board.MakeMove(6, 5, Check.State.Black);
		gameManager.board.MakeMove(7, 5, Check.State.Black);
		gameManager.board.MakeMove(8, 5, Check.State.Black);
		gameManager.board.MakeMove(9, 5, Check.State.Black);
		gameManager.board.MakeMove(5, 6, Check.State.Black);
		gameManager.board.MakeMove(9, 6, Check.State.Black);
		gameManager.board.MakeMove(5, 7, Check.State.Black);
		gameManager.board.MakeMove(9, 7, Check.State.Black);
		gameManager.board.MakeMove(5, 8, Check.State.Black);
		gameManager.board.MakeMove(9, 8, Check.State.Black);
		gameManager.board.MakeMove(5, 9, Check.State.Black);
		gameManager.board.MakeMove(6, 9, Check.State.Black);
		gameManager.board.MakeMove(7, 9, Check.State.Black);
		gameManager.board.MakeMove(8, 9, Check.State.Black);
		gameManager.board.MakeMove(9, 9, Check.State.Black);

		gameManager.updateGraphAndProjectedBoard();
		gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(8, 9).id, Check.State.Black);
		gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(5, 5).id, Check.State.Black);
		gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(5, 9).id, Check.State.Black);
		gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(9, 5).id, Check.State.Black);
	}
}