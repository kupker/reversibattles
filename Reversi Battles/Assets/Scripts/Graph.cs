using UnityEngine;
using System.Collections.Generic;
using System;


public class Vertex
{
    public int id;
    public Check.State owner = Check.State.None;
    public List<int> neighbours = new List<int>();
    public List<int> weakNeighbours = new List<int>();

    public void addNeighbour(int id)
    {
        neighbours.Add(id);
    }

    public void removeNeighbour(int id)
    {
        neighbours.Remove(id);
    }

    //public static bool operator ==(Vertex a, Vertex b)
    //{
    //    return a.id == b.id;
    //}

    public static implicit operator int(Vertex a)
    {
        return a.id;
    }

    public Vertex(int vertexId)
    {
        id = vertexId;
    }
}

public class Path : IComparable
{
    public List<int> vertices = new List<int>();
    public int distance = 0;// vertices.Count wasn't sufficient, sometimes custom value is needed (Dijkstra initial value)

    public int CompareTo(object other)
    {
        Path otherPath = other as Path;
        return distance - otherPath.distance;
    }

    public void addVertex(int vertexId)
    {
        vertices.Add(vertexId);
        distance++;
    }

    public Path(int firstVertex)
    {
        vertices.Add(firstVertex);
    }

    public Path(Path other)
    {
        vertices = new List<int>(other.vertices);
        distance = other.distance;
    }
}

public class Pair
{
    public int a, b;

    public Pair(int a, int b)
    {
        this.a = a;
        this.b = b;
    }
}

// Dijkstra vertex - special vertex remembering path from the source
public class DVertex : IComparable
{
    public int vertexId;
    public Path path;

    public DVertex(Vertex v)
    {
        path = new Path(v.id);
        vertexId = v.id;
        path.distance = int.MaxValue;
    }

    // comparing based on distance from the source
    public int CompareTo(object other)
    {
        DVertex otherDVertex = other as DVertex;
        return path.CompareTo(otherDVertex.path);
    }

    public override string ToString()
    {
        return "Vertex ID: " + vertexId + ", distance: " + path.distance;
    }

    public int getDistance()
    {
        return path.distance;
    }
}

public class Graph
{
    public List<Vertex> vertices;

    public Graph(int width, int height)
    {
        vertices = new List<Vertex>();

        for (int i = 0; i < width * height; i++)
        {
            vertices.Add(new Vertex(i));
        }
    }

    public Vertex findVertex(int id)
    {
        return vertices.Find(x => x == id);
    }

    //Dijkstra
    public Path findShortestPath(int src, int dst)
    {
        List<DVertex> visited = new List<DVertex>();
        List<DVertex> unvisited = new List<DVertex>();

        foreach (Vertex v in vertices)
        {
            unvisited.Add(new DVertex(v));
        }

        unvisited.Find(x => x.vertexId == src).path.distance = 0;
        unvisited.Sort();// sort based on distance


        while (unvisited.Count > 0 && unvisited[0].getDistance() != int.MaxValue)//second condition means, there are no further reachable vertices from the start vertex
        {
            // all neighbours of vertex with lowest distance
            foreach (int neighbourId in vertices.Find(x => x.id == unvisited[0].vertexId).neighbours)
            {
                DVertex neighbour = unvisited.Find(x => x.vertexId == neighbourId);
                if (neighbour != null && neighbour.path.distance > unvisited[0].path.distance + 1)
                {
                    neighbour.path = new Path(unvisited[0].path);// remember current path
                    neighbour.path.addVertex(neighbourId);// also add current vertex to the end of the path
                }
            }

            visited.Add(unvisited[0]);
            unvisited.Remove(unvisited[0]);
            unvisited.Sort();// sort based on distance
        }

        DVertex result = visited.Find(x => x.vertexId == dst);
        if (result == null)
        {
            return null;
        }
        //foreach (int vertex in result.path.vertices)
        //{
        //    Debug.Log("dijkstra: " + vertex);
        //}
        return result.path;
    }

    public List<Path> findMaximumCircle(int vertexId, Check.State newOwner)
    {
        List<Pair> candidates = new List<Pair>();// list of pairs
        List<Path> paths = new List<Path>();// list of paths
        Vertex currentVertex = vertices.Find(x => x == vertexId);

        foreach (int weakNeighbourA in currentVertex.weakNeighbours)
        {
            if (weakNeighbourA == -1 || vertices.Find(x => x == weakNeighbourA).owner != newOwner)// must be same owner
            {
                continue;
            }

            foreach (int weakNeighbourB in currentVertex.weakNeighbours)
            {//                          || don't add two identical vertices || add only vertices of given color                          || check, if the pair A-B hasn't already added as B-A before
                if (weakNeighbourB == -1 || weakNeighbourA == weakNeighbourB || vertices.Find(x => x == weakNeighbourB).owner != newOwner || candidates.Find(x => x.a == weakNeighbourB && x.b == weakNeighbourA) != null)
                {
                    continue;
                }
                //Debug.Log("adding candidate: " + weakNeighbourA + "-" + weakNeighbourB);
                candidates.Add(new Pair(weakNeighbourA, weakNeighbourB));
            }
        }

        if (candidates.Count == 0)
        {
            return paths;
        }

        processCorner(currentVertex.weakNeighbours[3], currentVertex.weakNeighbours[1], currentVertex.weakNeighbours[0], ref candidates, newOwner);// top left
        processCorner(currentVertex.weakNeighbours[1], currentVertex.weakNeighbours[4], currentVertex.weakNeighbours[2], ref candidates, newOwner);// top right
        processCorner(currentVertex.weakNeighbours[4], currentVertex.weakNeighbours[6], currentVertex.weakNeighbours[7], ref candidates, newOwner);// bottom right
        processCorner(currentVertex.weakNeighbours[6], currentVertex.weakNeighbours[3], currentVertex.weakNeighbours[5], ref candidates, newOwner);// bottom left

        foreach (Pair pair in candidates)
        {
            Path p = findShortestPath(pair.a, pair.b);
            if (p != null)
            {
                p.addVertex(vertexId);
                paths.Add(p);
            }
        }

        paths.Sort();
        return paths;
        //Path result = paths[paths.Count - 1];
        //if (result == null)
        //{
        //    return null;
        //}
        //return result;
    }

    // this method takes three neighbour vertices and checks them for containing corner triangle, if so, the corner vertex must be disabled
    // so it removes all pairs containing this corner from the candidates List
    private void processCorner(int diagonalA, int diagonalB, int corner, ref List<Pair> candidates, Check.State owner)
    {
        if (diagonalA > -1 && diagonalB > -1 && corner > -1
            && vertices.Find(x => x == diagonalA).owner == owner
            && vertices.Find(x => x == diagonalB).owner == owner
            && vertices.Find(x => x == corner).owner == owner)// corner triangle is present and owned by given color
        {
            List<Pair> candidatesCopy = new List<Pair>(candidates);
            foreach (Pair pair in candidatesCopy)// remove all pairs containing corner vertex
            {
                if (pair.a == corner || pair.b == corner)
                {
                    candidates.Remove(pair);
                }
            }

        }
    }

    public void generateWeakNeighbours(int width, int height)
    {
        if (height * width != vertices.Count)
        {
            return;
        }

        for (int i = 0; i < vertices.Count; i++)
        {
            Vertex currentVertex = vertices.Find(x => x == i);

            if (i % width != 0 && i >= width)// top left
            {
                currentVertex.weakNeighbours.Add(i - width - 1);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i >= width)// top mid
            {
                currentVertex.weakNeighbours.Add(i - width);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i >= width && i % width != width - 1)// top right
            {
                currentVertex.weakNeighbours.Add(i - width + 1);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i % width != 0)// left
            {
                currentVertex.weakNeighbours.Add(i - 1);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i % width != width - 1)// right
            {
                currentVertex.weakNeighbours.Add(i + 1);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i % width != 0 && i < vertices.Count - width)// bottom left
            {
                currentVertex.weakNeighbours.Add(i + width - 1);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i < vertices.Count - width)// bottom mid
            {
                currentVertex.weakNeighbours.Add(i + width);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }

            if (i % width != width - 1 && i < vertices.Count - width)// bottom right
            {
                currentVertex.weakNeighbours.Add(i + width + 1);
            }
            else
            {
                currentVertex.weakNeighbours.Add(-1);
            }
        }
    }

    public void addEdge(int a, int b)
    {
        findVertex(a).addNeighbour(b);
        findVertex(b).addNeighbour(a);
    }

    public void removeEdge(int a, int b)
    {
        findVertex(a).removeNeighbour(b);
        findVertex(b).removeNeighbour(a);
    }

    public void setVertexOwner(int vertexId, Check.State owner)
    {
        //Debug.Log("setVertexOwner: " + vertexId + ", " + owner);
        Vertex currentVertex = vertices.Find(x => x == vertexId);

        if (currentVertex == null)
        {
            Debug.Log("setVertexOwner: vertex not found");
            return;
        }

        if (currentVertex.owner == owner)
        {
            return;
        }

        currentVertex.owner = owner;

        // remove all current neighbours, itarete in copy of list,
        // because the original list is changing during the process
        foreach (int neighbour in new List<int>(currentVertex.neighbours))
        {
            removeEdge(vertexId, neighbour);
        }

        foreach (int weakNeighbour in currentVertex.weakNeighbours)
        {
            if (weakNeighbour > -1 && vertices.Find(x => x == weakNeighbour).owner == owner)//would it be strong neighbour?
            {
                addEdge(weakNeighbour, vertexId);
            }
        }
    }
}
