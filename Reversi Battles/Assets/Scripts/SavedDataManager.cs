﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class PlayerProfile
{
    public string playerName;
    public uint campaignProgress;
    public uint tutorialCompleted;
    public uint[] stars = new uint[5];

    public PlayerProfile(string playerName)
    {
        this.playerName = playerName;
        this.campaignProgress = 0;
        this.tutorialCompleted = 0;
    }

    public PlayerProfile(string playerName, uint campaignProgress, uint tutorialCompleted, uint[] stars)
    {
        this.playerName = playerName;
        this.campaignProgress = campaignProgress;
        this.tutorialCompleted = tutorialCompleted;
        this.stars = stars;
    }
}

public struct ScoreItem
{
    public string playerName;
    public uint score;

    public ScoreItem(string playerName, uint score)
    {
        this.playerName = playerName;
        this.score = score;
    }
}

public class SavedDataManager
{
    private const uint itemsInProfile = 8;
    private const uint itemsInScore = 2;
    private const uint itemsInGlobalSettings = 1;

    public static List<ScoreItem> scoreTable = new List<ScoreItem>();
    public static List<PlayerProfile> playerProfiles = new List<PlayerProfile>();
    public static int currentProfile = 0;

    public static void addScore(string playerName, uint score, bool saveToFile = false)
    {
        scoreTable.Add(new ScoreItem(playerName, score));

        if (saveToFile)
        {
            saveScoreTable();
        }
    }
    public static void createNewPlayer(string playerName, bool saveToFile = false)
    {
        playerProfiles.Add(new PlayerProfile(playerName));

        if (saveToFile)
        {
            saveProfiles();
        }
    }

    public static void addPlayerProfile(string playerName, uint campaignProgress, uint tutorialCompleted, uint[] stars, bool saveToFile = false)
    {
        playerProfiles.Add(new PlayerProfile(playerName, campaignProgress, tutorialCompleted, stars));

        if (saveToFile)
        {
            saveProfiles();
        }
    }

    public static void updatePlayerProfile(int profileIndex, uint campaignProgress, uint tutorialCompleted, uint[] stars, bool saveToFile = false)
    {
        if (profileIndex < 0 || profileIndex >= playerProfiles.Count)
        {
            return;
        }

        PlayerProfile profile = playerProfiles[profileIndex];
        profile.campaignProgress = campaignProgress;
        profile.tutorialCompleted = tutorialCompleted;
        profile.stars = stars;

        if (saveToFile)
        {
            saveProfiles();
        }
    }

    public static void removePlayerProfile(int profileIndex, bool saveToFile = false)
    {
        playerProfiles.RemoveAt(profileIndex);

        if (saveToFile)
        {
            saveProfiles();
        }
    }

    public static void setCurrentProfile(int profileIndex, bool saveToFile = false)
    {
        currentProfile = profileIndex;

        if (saveToFile)
        {
            saveSettings();
        }
    }

    private static uint[] parseStars(string[] starsStr, uint startIndex, uint count)
    {
        if (count < 1 || startIndex + count > starsStr.Length)
        {
            return null;
        }

        uint[] stars = new uint[count];

        for (uint i = startIndex; i < startIndex+count; i++)
        {
            uint.TryParse(starsStr[i], out stars[i - startIndex]);
        }

        return stars;
    }

    public static bool loadProfiles()
    {
        if (!File.Exists("profiles.dat"))
        {
            return false;
        }

        playerProfiles.Clear();
        string[] fileLines = File.ReadAllLines("profiles.dat");
        uint campaignProgress;
        uint tutorialCompleted;

        for (uint i = 0; i < fileLines.Length / itemsInProfile; i++)
        {
            uint.TryParse(fileLines[itemsInProfile * i + 1], out campaignProgress);
            uint.TryParse(fileLines[itemsInProfile * i + 2], out tutorialCompleted);
            addPlayerProfile(fileLines[itemsInProfile * i], campaignProgress, tutorialCompleted, parseStars(fileLines, itemsInProfile * i + 3, 5));
        }

        return true;
    }

    public static void saveProfiles()
    {
        string[] fileLines = new string[playerProfiles.Count * itemsInProfile];

        for (int i = 0; i < playerProfiles.Count; i++)
        {
            fileLines[itemsInProfile * i] = playerProfiles[i].playerName;
            fileLines[itemsInProfile * i + 1] = playerProfiles[i].campaignProgress.ToString();
            fileLines[itemsInProfile * i + 2] = playerProfiles[i].tutorialCompleted.ToString();
            fileLines[itemsInProfile * i + 3] = playerProfiles[i].stars[0].ToString();
            fileLines[itemsInProfile * i + 4] = playerProfiles[i].stars[1].ToString();
            fileLines[itemsInProfile * i + 5] = playerProfiles[i].stars[2].ToString();
            fileLines[itemsInProfile * i + 6] = playerProfiles[i].stars[3].ToString();
            fileLines[itemsInProfile * i + 7] = playerProfiles[i].stars[4].ToString();
        }

        File.WriteAllLines("profiles.dat", fileLines);
    }

    public static bool loadScoreTable()
    {
        if (!File.Exists("score.dat"))
        {
            return false;
        }

        scoreTable.Clear();
        string[] fileLines = File.ReadAllLines("score.dat");
        uint score;

        for (uint i = 0; i < fileLines.Length / itemsInScore; i++)
        {
            uint.TryParse(fileLines[itemsInScore * i + 1], out score);
            addScore(fileLines[itemsInScore * i], score);
        }

        return true;
    }

    public static void saveScoreTable()
    {
        string[] fileLines = new string[scoreTable.Count * itemsInScore];

        for (int i = 0; i < scoreTable.Count; i++)
        {
            fileLines[itemsInScore * i] = scoreTable[i].playerName;
            fileLines[itemsInScore * i + 1] = scoreTable[i].score.ToString();
        }

        File.WriteAllLines("score.dat", fileLines);
    }

    public static bool loadSettings()
    {
        if (!File.Exists("settings.dat"))
        {
            return false;
        }

        string[] fileLines = File.ReadAllLines("settings.dat");
        int.TryParse(fileLines[0], out currentProfile);

        return true;
    }

    public static void saveSettings()
    {
        string[] fileLines = new string[itemsInGlobalSettings];
        fileLines[0] = currentProfile.ToString();
        File.WriteAllLines("settings.dat", fileLines);
    }

    public static void loadSavedData()
    {
        loadProfiles();
        loadScoreTable();
        loadSettings();
        saveData();
    }

    public static void saveData()
    {
        saveProfiles();
        saveScoreTable();
        saveSettings();
    }
}
