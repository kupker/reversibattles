﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
using UnityEngine;

/// <summary>
/// Possible values for check on playing board.
/// </summary>
public class Check
{
    public enum State { None = 0, Black, White, Prohibited }

    public State state;	//internal state, represents which player owns the check (AI uses it)
    public State projectState;	//what is shown to user
    public bool occupied;	//was the check occupied?

    public bool processed;	//used when board is processed using seed filling algorithm

    public Check()
    {
        state = State.None;
        projectState = State.None;
        occupied = false;
        processed = false;
    }

    public Check(State state)
    {
        this.state = state;
        projectState = state;
        occupied = false;
        processed = false;
    }

    public Check(Check other)
    {
        state = other.state;
        projectState = other.projectState;
        occupied = other.occupied;
        processed = false;
    }

    public void Set(State state)
    {
        this.state = state;
        projectState = state;
    }

    public void Occupy(State state)
    {
        this.state = state;
        projectState = State.Prohibited;
        occupied = true;
    }
}

/// <summary>
/// Coordinates.
/// </summary>
public class Coordinates : IEquatable<Coordinates>
{
    public int x;
    public int y;

    public Coordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Coordinates(Coordinates other)
    {
        x = other.x;
        y = other.y;
    }

    public bool Equals(Coordinates other)
    {
        return this.x == other.x && this.y == other.y;
    }
}


/// <summary>
/// Move record.
/// </summary>
public class MoveRecord
{
    public enum Type { NormalMove, Occupy, SeedFillStart }

    public Coordinates coordinates;
    public Type type;
    public Check check;
    public Check originalCheck;

    public MoveRecord(Coordinates coordinates, Type type, Check check, Check originalCheck)
    {
        this.coordinates = coordinates;
        this.type = type;
        this.check = new Check(check);
        this.originalCheck = new Check(originalCheck);
    }

    public MoveRecord(Type type)
    {
        this.type = type;
    }
}


/// <summary>
/// Playing board.
/// </summary>
public class Board
{
    Check[,] board;
    int sizeX;
    int sizeY;

    // Number of each players checks (heuristic)
    public int[] numChecks;

    List<MoveRecord> moveHistory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Board"/> class.
    /// </summary>
    /// <param name="sizeX">Size x of board.</param>
    /// <param name="sizeY">Size y of board.</param>
    /// <param name="prohibited">If true sets all checks on board as prohibited</param>
    public Board(int sizeX, int sizeY, bool prohibited = false)
    {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        board = new Check[sizeX, sizeY];

        // Init board with default checks - none or prohibited
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                board[i, j] = new Check();
                if (prohibited)
                {
                    board[i, j].Set(Check.State.Prohibited);
                }
                else
                {
                    board[i, j].Set(Check.State.None);
                }
            }
        }

        SetBorder();

        // Init number of each players checks
        // numChecks[0] will never be used, it is done in this way,
        // so it can be used like this: numChecks[(int)stoneColor]
        numChecks = new int[3];
        for (int i = 0; i < 3; i++)
        {
            numChecks[i] = 0;
        }

        moveHistory = new List<MoveRecord>();
    }

    /// <summary>
    /// Initializing new instance of board using other board.
    /// </summary>
    /// <param name="other"></param>
    public Board(Board other)
    {
        sizeX = other.sizeX;
        sizeY = other.sizeY;

        board = new Check[sizeX, sizeY];

        // Init board with default checks - none or prohibited
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                board[i, j] = new Check(other.board[i, j]);
            }
        }

        // Init number of each players checks
        // numChecks[0] will never be used, it is done in this way,
        // so it can be used like this: numChecks[(int)stoneColor]
        numChecks = new int[3];
        for (int i = 0; i < 3; i++)
        {
            numChecks[i] = other.numChecks[i];
        }

        moveHistory = new List<MoveRecord>();
        foreach (var move in other.moveHistory)
        {
            moveHistory.Add(move);
        }
    }

    /// <summary>
    /// Copies the state of board.
    /// </summary>
    /// <param name="other"></param>
    public void CopyState(Board other)
    {
        sizeX = other.sizeX;
        sizeY = other.sizeY;

        // Init board with default checks - none or prohibited
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                board[i, j] = new Check(other.board[i, j]);
            }
        }

        // Init number of each players checks
        // numChecks[0] will never be used, it is done in this way,
        // so it can be used like this: numChecks[(int)stoneColor]
        for (int i = 0; i < 3; i++)
        {
            numChecks[i] = other.numChecks[i];
        }

        moveHistory.Clear();
        foreach (var move in other.moveHistory)
        {
            moveHistory.Add(move);
        }
    }

    /// <summary>
    /// Sets the border of board as prohibited.
    /// </summary>
    public void SetBorder()
    {
        for (int i = 0; i < sizeX; i++)
        {
            board[i, 0].Set(Check.State.Prohibited);
            board[i, sizeY - 1].Set(Check.State.Prohibited);
        }
        for (int i = 0; i < sizeY; i++)
        {
            board[0, i].Set(Check.State.Prohibited);
            board[sizeX - 1, i].Set(Check.State.Prohibited);
        }
    }

    /// <summary>
    /// Sets all prohibited checks as empty except for border.
    /// </summary>
    public void EnableAllProhibited()
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                if (board[i, j].state == Check.State.Prohibited)
                    board[i, j].Set(Check.State.None);
            }
        }

        SetBorder();
    }

    /// <summary>
    /// Determines whether the board is fully filled.
    /// </summary>
    /// <returns><c>true</c> if there is no more empty check; otherwise, <c>false</c>.</returns>
    public bool IsFilled()
    {
        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                if (board[i, j].state == Check.State.None)
                    return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Gets the last move.
    /// </summary>
    /// <returns>The last move.</returns>
    public Coordinates GetLastMove()
    {
        return moveHistory.Last().coordinates;
    }

    /// <summary>
    /// Gets the check.
    /// </summary>
    /// <returns>The check.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    public Check GetCheck(int x, int y)
    {
        if (x < 0 || y < 0 || x >= sizeX || y >= sizeY)
        {
            return null;
        }
        return board[x, y];
    }

    /// <summary>
    /// Gets the check.
    /// </summary>
    /// <returns>The check.</returns>
    /// <param name="coord">Coordinates.</param>
    public Check GetCheck(Coordinates coord)
    {
        return GetCheck(coord.x, coord.y);
    }

    /// <summary>
    /// Sets check on the board.
    /// </summary>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    /// <param name="value">Value.</param>
    public void SetBoard(int x, int y, Check.State value)
    {
        board[x, y].Set(value);
    }

    /// <summary>
    /// Players move - it sets the stone on board and if some territory was surrouded it is occupied.
    /// </summary>
	/// <returns>Number of enemie's occupied dots.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    /// <param name="value">Stone color.</param>
	public int MakeMove(int x, int y, Check.State value, bool occupy = true)
    {
        var originalCheck = new Check(board[x, y]);
        board[x, y].Set(value);
        numChecks[(int)value]++;
        moveHistory.Add(new MoveRecord(new Coordinates(x, y), MoveRecord.Type.NormalMove, new Check(board[x, y]), originalCheck));
		int result = 0;
		if (occupy)
            result = Occupy(new Coordinates(x, y));
		return result;
    }

    /// <summary>
    /// Reverts last move.
    /// </summary>
    public void GoOneMoveBack()
    {
        while (moveHistory.Last().type == MoveRecord.Type.Occupy)
        {
            var lastMove = moveHistory.Last();
            moveHistory.Remove(lastMove);
            board[lastMove.coordinates.x, lastMove.coordinates.y] = lastMove.originalCheck;
            numChecks[(int)lastMove.check.state]--;
            if (lastMove.originalCheck.state != Check.State.None)
            {
                numChecks[(int)lastMove.originalCheck.state]++;
            }
        }
        if (moveHistory.Last().type == MoveRecord.Type.NormalMove)
        {
            var lastMove = moveHistory.Last();
            moveHistory.Remove(lastMove);
            board[lastMove.coordinates.x, lastMove.coordinates.y] = lastMove.originalCheck;
            numChecks[(int)lastMove.check.state]--;
        }
    }

    /// <summary>
    /// Gets the size x.
    /// </summary>
    /// <value>The size x.</value>
    public int SizeX
    {
        get
        {
            return sizeX;
        }
    }

    /// <summary>
    /// Gets the size y.
    /// </summary>
    /// <value>The size y.</value>
    public int SizeY
    {
        get
        {
            return sizeY;
        }
    }

    /// <summary>
    /// Returns coordinates of checks above, bellow, right and left the check.
    /// </summary>
    /// <returns>The neigbors.</returns>
    /// <param name="check">Check.</param>
    List<Coordinates> GetNeigbors(Coordinates check)
    {
        var result = new List<Coordinates>();
        result.Add(new Coordinates(check.x - 1, check.y));
        result.Add(new Coordinates(check.x + 1, check.y));
        result.Add(new Coordinates(check.x, check.y - 1));
        result.Add(new Coordinates(check.x, check.y + 1));
        return result;
    }

    /// <summary>
    /// Returns coordinates of all adjoining checks.
    /// </summary>
    /// <returns>The all neighbors.</returns>
    /// <param name="check">Check.</param>
    public static List<Coordinates> GetAllNeighbors(Coordinates check)
    {
        var result = new List<Coordinates>();
        for (int i = check.x - 1; i <= check.x + 1; i++)
        {
            for (int j = check.y - 1; j <= check.y + 1; j++)
            {
                if (i != check.x || j != check.y)
                {
                    result.Add(new Coordinates(i, j));
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Returns all checks in distance 'distance' from check.
    /// </summary>
    /// <param name="check"></param>
    /// <param name="distance"></param>
    /// <returns></returns>
    public List<Coordinates> GetAllNeighborsInDistance(Coordinates check, int distance)
    {
        var result = new List<Coordinates>();
        for (int i = check.x - distance; i <= check.x + distance; i++)
        {
            for (int j = check.y - distance; j <= check.y + distance; j++)
            {
                if (i != check.x || j != check.y)
                {
                    if (i >= 0 && j >= 0 && i < sizeX && j < sizeY)
                    {
                        result.Add(new Coordinates(i, j));
                    }
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Other players color.
    /// </summary>
    /// <returns>The player.</returns>
    /// <param name="player">Player.</param>
    public static Check.State OtherPlayer(Check.State player)
    {
        if (player == Check.State.Black)
            return Check.State.White;
        else
            return Check.State.Black;
    }

    /// <summary>
    /// Returns true if check neighbors with at least x neigbors of color.
    /// </summary>
    /// <returns>The least X neighbors of color.</returns>
    /// <param name="check">Check.</param>
    /// <param name="x">The x coordinate.</param>
    /// <param name="color">Color.</param>
    public bool AtLeastXNeighborsOfColor(Coordinates check, int x, Check.State color)
    {
        int i = 0;
        var adjoining = GetAllNeighbors(check);
        foreach (var a in adjoining)
        {
            if (board[a.x, a.y].state == color)
            {
                i++;
                if (i >= x)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// If some territory was surrounded occupy it.
    /// </summary>
	/// <returns>Number of enemie's occupied dots.</returns>
    /// <param name="seed">Coordinates of last move.</param>
    int Occupy(Coordinates seed)
    {
		int result = 0;

        if (!AtLeastXNeighborsOfColor(seed, 2, board[seed.x, seed.y].state))
        {
			return result;
        }

        // Using seed filling algorithm determine if some territory was surronded.
        // For every neigboring empty check start seed filling algorithm and
        // if it do not hit board boundaries it means that territory was surrounded.
        var playerColor = board[seed.x, seed.y].state;
        var adjoining = GetAllNeighbors(seed);
        List<List<Coordinates>> filling = new List<List<Coordinates>>();
        foreach (var a in adjoining)
        {
            if (board[a.x, a.y].state != playerColor)
            {
                var stop = new MoveRecord(MoveRecord.Type.SeedFillStart);
                moveHistory.Add(stop);

                var hitBoundaries = false;
                var fill = SeedFill(a, playerColor, out hitBoundaries);

                //revert
                while (moveHistory.Last() != stop)
                {
                    GoOneMoveBack();
                }
                moveHistory.Remove(stop);

                if (!hitBoundaries)
                {
                    filling.Add(fill);
                }
            }

        }

        // Fill the surrounded terrritory with stones.
        foreach (var x in filling)
        {
            foreach (var f in x)
            {
                if (board[f.x, f.y].state == Check.State.None)
                {
                    numChecks[(int)playerColor]++;
                }
                else if (board[f.x, f.y].state != playerColor)
                {
					result++;
                    numChecks[(int)playerColor]++;
                    numChecks[(int)OtherPlayer(playerColor)]--;
                }

                var originalCheck = new Check(board[f.x, f.y]);
                board[f.x, f.y].Occupy(playerColor);
                moveHistory.Add(new MoveRecord(f, MoveRecord.Type.Occupy, board[f.x, f.y], originalCheck));
            }
        }

		return result;
    }

    /// <summary>
    /// Seed filling algorithm.
    /// </summary>
    /// <returns>Retuns filled territory.</returns>
    /// <param name="array">Board with players stones.</param>
    /// <param name="check">Coordinate of seed.</param>
    /// <param name="hitBoundaries">Hit boundaries.</param>
    List<Coordinates> SeedFill(Coordinates check, Check.State playerColor, out bool hitBoundaries)
    {
        var result = new List<Coordinates>();
        hitBoundaries = false;
        //array[check.x, check.y] = Check.White;
        MakeMove(check.x, check.y, playerColor, false);
        result.Add(check);
        var adjoining = GetNeigbors(check);
        foreach (var a in adjoining)
        {
            if (OutOfBoundaries(a))
            {
                hitBoundaries = true;
                return result;
            }
            if (board[a.x, a.y].state != playerColor)
            {
                result.AddRange(SeedFill(a, playerColor, out hitBoundaries));
                if (hitBoundaries)
                {
                    return result;
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Returns color of player that won, if it is not the end of the game none is returned.
    /// </summary>
	public Check.State Winner(int bonusWhite, int bonusBlack)
    {
        int whites = 0;
        int blacks = 0;
        int prohibited = 0;
        CountFinishedChecks(new Coordinates(0, 0), Check.State.Black, ref blacks, ref prohibited);
        FinishProcessing();
        prohibited = 0;
        CountFinishedChecks(new Coordinates(0, 0), Check.State.White, ref whites, ref prohibited);
        FinishProcessing();
        int others = SizeX * SizeY - whites - blacks - prohibited;
		if (others * 2 + blacks + bonusBlack < whites + bonusWhite)
        {
            return Check.State.White;
        }
		else if (others * 2 + whites + bonusWhite < blacks + bonusBlack)
        {
            return Check.State.Black;
        }
        else
        {
            return Check.State.None;
        }
    }

    /// <summary>
    /// Sets all checks as not processed.
    /// </summary>
    void FinishProcessing()
    {
        foreach (var c in board)
        {
            c.processed = false;
        }
    }

    /// <summary>
    /// Counts number of finished checks using seed filling algorithm.
    /// Finished means it is connected with border of board, that means it can not be
    /// occupied anymore.
    /// </summary>
    /// <param name="check">Start check.</param>
    /// <param name="whites">Whites.</param>
    /// <param name="blacks">Blacks.</param>
    /// <param name="prohibited">Prohibited.</param>
    void CountFinishedChecks(Coordinates check, Check.State color, ref int checks, ref int prohibited)
    {
        board[check.x, check.y].processed = true;
        if (board[check.x, check.y].state == color)
        {
            checks++;
        }
        else if (board[check.x, check.y].state == Check.State.Prohibited)
        {
            prohibited++;
        }
        var adjoining = GetNeigbors(check);
        foreach (var a in adjoining)
        {
            if (!OutOfBoundaries(a))
            {
                if (!board[a.x, a.y].processed &&
                    (board[a.x, a.y].state == color ||
                    board[a.x, a.y].state == Check.State.Prohibited))
                {
                    CountFinishedChecks(a, color, ref checks, ref prohibited);
                }
            }
        }
    }

    /// <summary>
    /// Returns true if coordinates are out of board.
    /// </summary>
    /// <param name="check">Coordinates.</param>
    public bool OutOfBoundaries(Coordinates check)
    {
        return check.x < 0 || check.x >= sizeX || check.y < 0 || check.y >= sizeY;
    }
}
