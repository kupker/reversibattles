﻿using UnityEngine;
using System.Collections;

public class SkirmishManager : MonoBehaviour {

	GameManager gameManager;

	void Start()
	{
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		gameManager.madeMoveCallback = new MadeMoveCallback(MadeMove);
	}

	public void MadeMove()
	{
		if (gameManager.gameEnd)
		{
			gameManager.isDisabled = true;
			MainMenu mainMenu = GameObject.Find ("MenuManager").GetComponent<MainMenu> ();
			mainMenu.showPostGameWindow ();
		}
	}
}
