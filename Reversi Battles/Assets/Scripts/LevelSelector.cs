﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelSelector : MonoBehaviour
{
    private GameObject[] buttons = new GameObject[6];

    private void showStars(uint buttonId, uint count)
    {
        if (count > 3 || buttonId > 5)
        {
            return;
        }

        for (uint i = 0; i < count; i++)
        {
            buttons[buttonId].GetComponentsInChildren<CanvasGroup>()[i].alpha = 1.0f;
        }
    }
    void Start()
    {
        uint progress = SavedDataManager.playerProfiles[SavedDataManager.currentProfile].campaignProgress;
        buttons[0] = GameObject.Find("TutorialBtn2");
        buttons[1] = GameObject.Find("Level1Btn");
        buttons[2] = GameObject.Find("Level2Btn");
        buttons[3] = GameObject.Find("Level3Btn");
        buttons[4] = GameObject.Find("Level4Btn");
        buttons[5] = GameObject.Find("Level5Btn");

        buttons[5].GetComponent<Button>().interactable = (progress >= 4);
        buttons[4].GetComponent<Button>().interactable = (progress >= 3);
        buttons[3].GetComponent<Button>().interactable = (progress >= 2);
        buttons[2].GetComponent<Button>().interactable = (progress >= 1);

        for (uint i = 0; i < 5; i++)
        {
            showStars(i + 1, SavedDataManager.playerProfiles[SavedDataManager.currentProfile].stars[i]);
        }
    }

}
