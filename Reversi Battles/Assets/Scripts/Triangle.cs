using UnityEngine;
using System.Collections;

public class Triangle : MonoBehaviour
{
    public Color color;
    public Check.State owner = Check.State.None;
    public int id;
    // Use this for initialization
    void Start()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        //gameObject.GetComponent<MeshRenderer>().material.color = color;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setOwner(Check.State owner)
    {
        this.owner = owner;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<MeshRenderer>().material.color = (owner == Check.State.Black) ? new Color(1, 0, 0, 0.5f) : new Color(0, 0, 1, 0.5f);
        //this.color = color;
    }

    void OnMouseEnter()
    {

    }

    void OnMouseDown()
    {
        //Debug.Log("triangle id: " + id);
        //GameObject.Find("ProjectedBoard").GetComponent<ProjectedBoard>().fillArea(id, Check.State.Black);
    }

    public int getNeighbourDot(int dotType, Vector2 boardSize)
    {
        int block = id - id % 4;

        if (dotType == 0)
        {
            return block / 4 + block / 4 / (int)boardSize.x;
        }
        else if (dotType == 1)
        {
            return block / 4 + block / 4 / (int)boardSize.x + 1;
        }
        else if (dotType == 2)
        {
            return block / 4 + block / 4 / (int)boardSize.x + (int)boardSize.x + 2;
        }
        else if (dotType == 3)
        {
            return block / 4 + block / 4 / (int)boardSize.x + (int)boardSize.x + 1;
        }

        return -2;
    }

    public int getNeighbour(int neigbourType, Vector2 boardSize)
    {
        int block = id - id % 4;

        if (neigbourType == 1)
        {
            return (id + 1) % 4 + block;
        }
        else if (neigbourType == 3)
        {
            return (id + 3) % 4 + block;
        }
        else if (neigbourType == -2)
        {
            if (id % 4 == 3 && id < boardSize.x * 4 ||
                id % 4 == 1 && id > boardSize.x * (boardSize.y - 1) * 4 ||
                id % 4 == 0 && id % (int)(boardSize.x * 4) == 0 ||
                id % 4 == 2 && id % (int)(boardSize.x * 4) == boardSize.x * 4 - 2)
            {
                return -1;
            }
            else if (id % 4 == 2)
            {
                return id + 2;
            }
            else if (id % 4 == 0)
            {
                return id - 2;
            }
            else if (id % 4 == 3)
            {
                return id - (int)boardSize.x * 4 - 2;
            }
            else if (id % 4 == 1)
            {
                return id + (int)boardSize.x * 4 + 2;
            }
        }

        return -2;
    }

    public bool isBorder(int width, int height)
    {
        return ((id % (4 * (width - 1)) == 0) ||
            (id % (4 * (width - 1)) == 4 * (width - 1) - 2) ||
            (id % 4 == 3 && id < 4 * (width - 1)) ||
            (id % 4 == 1 && id > 4 * (width - 1) * (height - 2)));
    }
}
