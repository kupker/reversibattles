﻿using UnityEngine;
using System.Collections;

public class CameraBahaviour : MonoBehaviour {

	public float speed = 0;
	public float treshold = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (speed != 0 && GetComponent<Camera>().orthographicSize < treshold) {
			GetComponent<Camera>().orthographicSize += speed; 
		}
	}
}
