﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class Level1Manager : LevelManager
{

    protected override void SetFirstStage()
    {
        gameManager.board.MakeMove(4, 4, Check.State.Black);
		gameManager.board.MakeMove(4, 5, Check.State.Black);
		gameManager.board.MakeMove(5, 4, Check.State.Black);
		gameManager.board.MakeMove(5, 5, Check.State.Black);

		gameManager.board.MakeMove(7, 4, Check.State.Black);
		gameManager.board.MakeMove(7, 5, Check.State.Black);
		gameManager.board.MakeMove(8, 4, Check.State.Black);
		gameManager.board.MakeMove(8, 5, Check.State.Black);

        gameManager.updateGraphAndProjectedBoard();
    }
}