﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class Level2Manager : LevelManager
{

	protected override void SetFirstStage()
	{
		gameManager.board.MakeMove(7, 4, Check.State.Black);
		gameManager.board.MakeMove(8, 5, Check.State.Black);
		gameManager.board.MakeMove(9, 6, Check.State.Black);
		gameManager.board.MakeMove(8, 7, Check.State.Black);
		gameManager.board.MakeMove(7, 7, Check.State.Black);
		gameManager.board.MakeMove(6, 7, Check.State.Black);
		gameManager.board.MakeMove(5, 6, Check.State.Black);
		gameManager.board.MakeMove(6, 5, Check.State.Black);

		gameManager.updateGraphAndProjectedBoard();
		gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(6, 5).id, Check.State.Black);
	}
}