﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

	protected GameManager gameManager;
	protected GameObject canvas;
	protected MainMenu guiManager;

	public int state = 0;
	protected bool initialized = false;

	//float delayCounter = 0;

	// Use this for initialization
	void Start()
	{
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		canvas = GameObject.Find("Canvas");
		guiManager = GameObject.Find("MenuManager").GetComponent<MainMenu>();
	}

	void Update()
	{
		if (!initialized)
		{
			gameManager.madeMoveCallback = new MadeMoveCallback(MadeMove);
			SetFirstStage();
			initialized = true;
			state = 2;
			NextState ();
		}

		if (ShowingText() && Input.GetMouseButtonDown(0))
		{
			NextState();
		}

		if (Input.GetMouseButtonDown(1))
		{
			//GameObject.Find ("ScoreTextBlue").GetComponent<Text>().text = 63.ToString();
			//gameManager.board.numChecks [2] = 165;
			NextState ();
			//            if (guiManager)
			//            {
			//                guiManager.showPostGameWindow();
			//            }
		}
	}

	bool ShowingText()
	{
		switch (state)
		{
		case 0:
		case 1:
		case 2:
		case 4:
		case 5:
			return true;
		default:
			return false;
		}
	}

	void NextState()
	{
		switch (state)
		{
		case 0:
			canvas.transform.Find("Text").GetComponent<Text>().text = "Text1";
			break;
		case 1:
			canvas.transform.Find("Text").GetComponent<Text>().text = "Text2";
			break;
		case 2:
			//canvas.SetActive (false);
			gameManager.isDisabled = false;
			gameManager.updateGraphAndProjectedBoard ();
			gameManager.switchPlayers = true;
			gameManager.isAiEnabled = true;
			gameManager.AiEasy = true;
			break;
		case 3:
			//                canvas.SetActive(true);
			gameManager.isDisabled = true;
			//                canvas.transform.Find("Text").GetComponent<Text>().text = "Perfect.";
			MainMenu mainMenu = GameObject.Find ("MenuManager").GetComponent<MainMenu> ();
			mainMenu.showPostGameWindow ();
			break;
		case 4:
			break;
		case 5:
			//                canvas.SetActive(false);
			//                	SceneManager.LoadScene(0);
			break;
		}
		state++;
	}

	protected virtual void SetFirstStage ()
	{

	}

	public void MadeMove()
	{
		if (!ShowingText())
		{
			if (gameManager.gameEnd)
			{
				NextState();
			}
		}
	}
}