using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Dot : MonoBehaviour
{

    //public Color[] CheckImages;
    public int coordX, coordY;
    public int id = 0;

    public Check.State check = Check.State.None;
    GameManager gameManager;
	Animator animator;

	bool pulsating;

	public bool Pulsating {
		get {
			return pulsating;
		}
		set {
			pulsating = value;
			animator.SetBool ("Pulsate", pulsating);
		}
	}

    // Use this for initialization
    void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		animator = GetComponent<Animator> ();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnMouseDown()
    {
        if (!gameManager.isDisabled)
        {
            //Debug.Log("id: " + id);
            //getNeighbourTriangles(15, 15);


            if (check == Check.State.None)
            {
				gameManager.StopPulsation ();
				Pulsating = true;
				//animator.SetTrigger("OnClick");
                gameManager.MakeMove(id);
            }
        }
    }

    void OnMouseEnter()
    {
        if (!gameManager.isDisabled)
        {
            if (check == Check.State.None)
            {	
                gameObject.GetComponent<Renderer>().material.color = gameManager.playerColors[gameManager.PlayerOnTheMove - 1];
            }


            //Debug.Log ("" + coordX + " " + coordY);
            //GameObject.Find("Text").GetComponent<Text>().text = "ID: " + id;
            //foreach (int weakNeighbour in gameManager.graph.findVertex(id).weakNeighbours)
            //{
            //    gameManager.findDot(weakNeighbour).GetComponent<Renderer>().material.color = Color.blue;
            //}
            //Debug.Log("id: " + id);
            //getNeighbourTriangles(15, 15);
            //print(gameObject.transform.position.x + " " + gameObject.transform.position.y + " " + gameObject.transform.position.z);
        }
    }

    void OnMouseExit()
    {
        updateColor();

        //foreach (int weakNeighbour in gameManager.graph.findVertex(id).weakNeighbours)
        //{
        //    gameManager.findDot(weakNeighbour).updateColor();
        //}
    }

    public void ChangeState(Check.State state)
    {
        check = state;
        updateColor();
    }

    public void updateColor()
    {
        //gameObject.SetActive(true);
		if (check != Check.State.Prohibited)
        	gameObject.GetComponent<MeshRenderer>().enabled = true;
        switch ((int)check)
        {
            case 0:
                gameObject.GetComponent<Renderer>().material.color = Color.white;
                break;
            case 1:// convert to player 1 stone
                gameObject.GetComponent<Renderer>().material.color = gameManager.playerColors[0];
                break;
            case 2:// convert to player 2 stone
                gameObject.GetComponent<Renderer>().material.color = gameManager.playerColors[1];
                break;
            case 3:
                //gameObject.GetComponent<Renderer>().material.color = Color.green;
                //gameObject.SetActive(false);
                //gameObject.GetComponent<MeshRenderer>().enabled = false;
                break;
            default:
                break;

        }
    }

    public List<int> getNeighbourTriangles(int boardWidth, int boardHeight)
    {
        if (boardWidth < 1 || boardHeight < 1)
        {
            return null;
        }

        List<int> neighbourTriangles = new List<int>();
        int row = id / boardWidth;

        if (id % boardWidth != boardWidth - 1 && id < (boardWidth - 1) * boardHeight)
        {
            neighbourTriangles.Add((id - row) * 4);
            neighbourTriangles.Add((id - row) * 4 + 3);
        }

        if (id % boardWidth != boardWidth - 1 && id > boardWidth - 1)
        {
            neighbourTriangles.Add((id - boardWidth - row + 1) * 4);
            neighbourTriangles.Add((id - boardWidth - row + 1) * 4 + 1);
        }

        if (id % boardWidth != 0 && id < (boardWidth - 1) * boardHeight)
        {
            neighbourTriangles.Add((id - 1 - row) * 4 + 2);
            neighbourTriangles.Add((id - 1 - row) * 4 + 3);
        }

        if (id % boardWidth != 0 && id > boardWidth - 1)
        {
            neighbourTriangles.Add((id - boardWidth - row) * 4 + 1);
            neighbourTriangles.Add((id - boardWidth - row) * 4 + 2);
        }

        //foreach (int neighbourId in neighbourTriangles)
        //{
        //    //Debug.Log("neigbour id: " + neighbourId);
        //    gameManager.projectedBoard.triangles[neighbourId].setOwner(Check.State.Black);
        //}

        return neighbourTriangles;
    }
}
