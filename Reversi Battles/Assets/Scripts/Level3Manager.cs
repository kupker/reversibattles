﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class Level3Manager : LevelManager
{

	protected override void SetFirstStage()
	{
		gameManager.board.MakeMove(11, 7, Check.State.Black);
		gameManager.board.MakeMove(12, 7, Check.State.Black);
		gameManager.board.MakeMove(5, 7, Check.State.Black);
		gameManager.board.MakeMove(6, 7, Check.State.Black);
		gameManager.board.MakeMove(7, 7, Check.State.Black);
		gameManager.board.MakeMove(8, 7, Check.State.Black);
		gameManager.board.MakeMove(9, 7, Check.State.Black);
		gameManager.board.MakeMove(10, 7, Check.State.Black);

		gameManager.updateGraphAndProjectedBoard();
		//gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(6, 5).id, Check.State.Black);
	}
}