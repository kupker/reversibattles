using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine.UI;
using System;

public delegate void MadeMoveCallback();

public class GameManager : MonoBehaviour
{

    public int PlayerOnTheMove = 1;
    public Board board;
    public ProjectedBoard projectedBoard;
    //public Dot[,] projectedBoard;
    public Color[] playerColors;
	public int[] bonusScore;

    public bool finishedInit = false;
    public int sizeX;
    public int sizeY;
    public bool filled;
    public bool switchPlayers;
    public bool isAiEnabled;
    public bool AiEasy;
    public bool isDisabled;

    public MadeMoveCallback madeMoveCallback;
    public Graph graph;
	

	public AudioClip audioSelect;
	public AudioClip audioOccupy;
	AudioSource audioSource;

	public bool gameEnd = false;

    //debug
    public GameObject orange;

    // Use this for initialization
    void Start()
    {
        board = new Board(sizeX, sizeY, filled);
        graph = new Graph(sizeX, sizeY);
        projectedBoard = GameObject.Find("ProjectedBoard").GetComponent<ProjectedBoard>();
        graph.generateWeakNeighbours(sizeX, sizeY);
        projectedBoard.GenerateDots(board);
        updateGraphAndProjectedBoard();
        finishedInit = true;
		bonusScore = new int[] { 0, 0, 0 };
		audioSource = GetComponent<AudioSource> ();
        MainMenu.gameManager = gameObject;
    }

    void Update()
    {
        // @Jen: following 2 lines should be called just once per move, this approach is inefficient
		MainMenu.scoreTextRed.GetComponent<Text>().text = (board.numChecks [1] + bonusScore[1]).ToString();
        MainMenu.scoreTextBlue.GetComponent<Text>().text = (board.numChecks[2] + bonusScore[2]).ToString();
        //Debug.DrawRay(new Vector3(0, 0), new Vector3(0, 0, -10));
        //ProjectPossibleMoves();
        //GameObject.Find("Text").GetComponent<Text>().text = "" + board.numChecks[1] + "x" + board.numChecks[2];
        //if (debugAi != null && debugAi.debugActivce)
        //{
        //    ProjectBoard(debugAi.debugBoard);
        //} else
        //{
        //    ProjectBoard(board);
        //}
    }

    void OnDestroy()
    {
        MainMenu.gameManager = null;
    }

    //public void ProjectBoard()
    //{
    //    for (int i = 0; i < board.SizeX; i++)
    //    {
    //        for (int j = 0; j < board.SizeY; j++)
    //        {
    //            projectedBoard[i, j].ChangeState(graph.findVertex(projectedBoard[i, j].id).owner);
    //        }
    //    }

    //    foreach (Vertex v in graph.vertices)
    //    {
    //        Debug.Log("Edges of: " + v.id);
    //        foreach (int neighbour in v.neighbours)
    //        {
    //            Debug.Log("Edge: " + v.id + "-" + neighbour);
    //        }
    //    }
    //}


    //void ProjectPossibleMoves()
    //{
    //    foreach (var x in GameObject.FindGameObjectsWithTag("UnderBoard"))
    //    {
    //        Destroy(x);
    //    }

    //    foreach (var pm in board.possibleMoves)
    //    {
    //        var o = Instantiate(orange);
    //        o.transform.position = projectedBoard[pm.x, pm.y].transform.position;
    //    }
    //}

    public void SwitchPlayers()
    {
        PlayerOnTheMove = PlayerOnTheMove % 2 + 1;
    }

    public void AIMove()
    {
        if (!board.IsFilled())
        {
            var ai = new AI((Check.State)PlayerOnTheMove, AiEasy);
            ai.boardState = board;
            ai.NextMove();
            var aiMove = ai.nextMove;
            createCircles(projectedBoard.findDot(aiMove.x, aiMove.y).id);
            int bonus = board.MakeMove(aiMove.x, aiMove.y, (Check.State)PlayerOnTheMove);
			bonusScore [PlayerOnTheMove] += bonus;
            updateGraphAndProjectedBoard();
			projectedBoard.fillArea(projectedBoard.findDot(aiMove.x, aiMove.y).id, (Check.State)PlayerOnTheMove);
            Dot currentDot = projectedBoard.findDot(aiMove.x, aiMove.y);
            //currentDot.GetComponent<Animator>().SetTrigger("OnClick");
			StopPulsation ();
			currentDot.Pulsating = true;
            //			if (madeMoveCallback != null)
            //			{
            //				madeMoveCallback();
            //			}
			isDisabled = false;
            SwitchPlayers();
        }
    }

    public void MakeMove(int dotId)
    {
		
        Dot currentDot = projectedBoard.findDot(dotId);
        createCircles(dotId);

        int bonus = board.MakeMove(currentDot.coordX, currentDot.coordY, (Check.State)PlayerOnTheMove);
		if (bonus > 0)
			audioSource.PlayOneShot (audioOccupy);
		else
			audioSource.PlayOneShot (audioSelect);
		bonusScore [PlayerOnTheMove] += bonus;
        updateGraphAndProjectedBoard();
        projectedBoard.fillArea(dotId, (Check.State)PlayerOnTheMove);
        //graph.setVertexOwner(dotId, (Check)PlayerOnTheMove);
        //currentDot.ChangeState((Check)PlayerOnTheMove);

		CheckGameEnd ();

        if (madeMoveCallback != null)
        {
            madeMoveCallback();
        }

		if (!isDisabled) {
			if (switchPlayers) {
				SwitchPlayers ();
			}

			if (isAiEnabled && !gameEnd) {
				isDisabled = true;
				Invoke ("AIMove", 1);
			}
		}
    }

	void CheckGameEnd()
	{
		if (board.Winner(bonusScore[2], bonusScore[1]) != Check.State.None)
		{
			gameEnd = true;
		}
	}

    public void updateGraphAndProjectedBoard()
    {
        Dot currentDot;
        Check.State currentCheck;

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                currentDot = projectedBoard.findDot(x, y);
                currentCheck = board.GetCheck(x, y).projectState;
                currentDot.ChangeState(currentCheck);
                graph.setVertexOwner(currentDot.id, currentCheck);
            }
        }
    }

	public void StopPulsation() {
		foreach (Dot dot in projectedBoard.dots) {
			dot.Pulsating = false;
		}
	}

    private void createCircles(int dotId)
    {
        List<Path> circles = graph.findMaximumCircle(dotId, (Check.State)PlayerOnTheMove);

        foreach (Path circle in circles)
        {
            projectedBoard.createCircle(circle);
        }
    }

    public void EnableAllProhibited()
    {
        board.EnableAllProhibited();
        projectedBoard.ProhibitHiddenDots(board);
    }
}
