﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class Level4Manager : LevelManager
{

	protected override void SetFirstStage()
	{
		gameManager.board.MakeMove(9, 5, Check.State.Black);
		gameManager.board.MakeMove(8, 6, Check.State.Black);
		gameManager.board.MakeMove(10, 6, Check.State.Black);
		gameManager.board.MakeMove(7, 7, Check.State.Black);
		gameManager.board.MakeMove(11, 7, Check.State.Black);
		gameManager.board.MakeMove(8, 8, Check.State.Black);
		gameManager.board.MakeMove(10, 8, Check.State.Black);
		gameManager.board.MakeMove(9, 9, Check.State.Black);
		//gameManager.board.MakeMove(10, 7, Check.State.Black);

		gameManager.updateGraphAndProjectedBoard();
		gameManager.projectedBoard.fillArea(gameManager.projectedBoard.findDot(9, 9).id, Check.State.Black);
	}
}