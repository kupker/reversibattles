﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartupDataLoaderScript : MonoBehaviour
{
    void Start()
    {
        DontDestroyOnLoad(GameObject.Find("IngameGUI"));
        DontDestroyOnLoad(GameObject.Find("MenuManager"));
        MainMenu.profileMenu = GameObject.Find("ProfileMenu");
        MainMenu.menuManager = GameObject.Find("MenuManager");
        MainMenu.profileList = GameObject.Find("ProfileList");
        MainMenu.campaignBtn = GameObject.Find("CampaignBtn");
        MainMenu.quickPlayBtn = GameObject.Find("QuickPlayBtn");
        MainMenu.scoreTextRed = GameObject.Find("ScoreTextRed");
        MainMenu.scoreTextBlue = GameObject.Find("ScoreTextBlue");
        MainMenu.postGameWindow = GameObject.Find("PostGameWindow");
        MainMenu.playerNameOnPanel = GameObject.Find("PlayerNameOnPanel");
        SavedDataManager.loadSavedData();

        if (SavedDataManager.playerProfiles.Count < 1)
        {
            MainMenu.menuManager.GetComponent<MainMenu>().showProfileMenu();
        }
        else if (MainMenu.profileMenu)
        {
            MainMenu.menuManager.GetComponent<MainMenu>().selectPlayerProfile(SavedDataManager.currentProfile);
            MainMenu.menuManager.GetComponent<MainMenu>().applySelectedProfile();
        }
    }
}
